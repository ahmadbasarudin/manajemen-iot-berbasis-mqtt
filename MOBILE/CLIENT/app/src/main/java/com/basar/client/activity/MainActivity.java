package com.basar.client.activity;

import android.os.Bundle;

import com.basar.client.R;

public class MainActivity extends BasarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    void initComponent() {

    }

    @Override
    void initVariable() {

    }

    @Override
    void initEvent() {

    }

    @Override
    void mainLogic() {

    }
}