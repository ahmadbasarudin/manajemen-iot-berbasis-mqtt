package com.basar.client.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.basar.client.R;

public class ErrorActivity extends BasarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
    }

    @Override
    void initComponent() {

    }

    @Override
    void initVariable() {

    }

    @Override
    void initEvent() {

    }

    @Override
    void mainLogic() {

    }
}