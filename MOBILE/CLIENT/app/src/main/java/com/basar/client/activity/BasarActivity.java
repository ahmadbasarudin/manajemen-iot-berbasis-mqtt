package com.basar.client.activity;


import androidx.appcompat.app.AppCompatActivity;

abstract class BasarActivity extends AppCompatActivity
{
    abstract  void initComponent();
    abstract  void initVariable();
    abstract  void initEvent();
    abstract  void mainLogic();
}
