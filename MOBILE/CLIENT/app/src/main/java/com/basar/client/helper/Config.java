package com.basar.client.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Config
{

    /** Pendeklarasian key-data berupa String, untuk sebagai wadah penyimpanan data.
     * Jadi setiap data mempunyai key yang berbeda satu sama lain */
    static final String REALNAME            ="REALNAME";
    static final String APP_TOKEN           = "APP-TOKEN";
    static final String DOMAIN              = "DOMAIN";
    static final String REGISTRATION_TOKEN  = "REGISTRATION-TOKEN";
    //0 1 dan 2
    static final String REGISTRATION_STATUS = "REGISTRATION-STATUS";

    //increment untuk versi
    static final Integer APP_VERSION        = 1;
    //nama untuk versi
    static final String  APP_NAME           = "simple_client";
    /*
        protocol
        http:// atau https://
     */
    static  final String APP_PROTOCOL       = "http://";


    //lama maksimal dealy 27detik
    static final Integer APP_VOLLEY_MAX_RESPONSE = 15000;

    //MQTT
    static final  String MQTT_SERVER        =   "MQTT-SERVER";



    /** Pendlakarasian Shared Preferences yang berdasarkan paramater context */
    public static SharedPreferences getSharedPreference(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key realname dengan parameter _realname */
    public static void setRealname(Context context, String _realname)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(REALNAME, _realname);
        editor.apply();
    }
    /** Mengembalikan nilai dari key user berupa String */
    public static String getRealname(Context context)
    {
        return getSharedPreference(context).getString(REALNAME,"");
    }

    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key domain dengan parameter _token_id */
    public static void setDomain(Context context, String _domain)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(DOMAIN, _domain);
        editor.apply();
    }
    /** Mengembalikan nilai dari key domain berupa String */
    public static String getDomain(Context context){
        return getSharedPreference(context).getString(DOMAIN,"");
    }

    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key token dengan parameter _token_id */
    public static void setRegistrationToken(Context context, String _registration_token)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(REGISTRATION_TOKEN, _registration_token);
        editor.apply();
    }
    /** Mengembalikan nilai dari key token berupa String */
    public static String getRegistrationToken(Context context)
    {
        return getSharedPreference(context).getString(REGISTRATION_TOKEN,"");
    }


    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key registration status dengan parameter _registration_status */
    public static void setRegistrationStatus(Context context, String _registration_status)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(REGISTRATION_STATUS, _registration_status);
        editor.apply();
    }
    /** Mengembalikan nilai dari key register status berupa String */
    public static String getRegistrationStatus(Context context)
    {
        return getSharedPreference(context).getString(REGISTRATION_STATUS,"");
    }


    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key app token dengan parameter _registration_status */
    public static void setAppToken(Context context, String _app_token)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(APP_TOKEN, _app_token);
        editor.apply();
    }
    /** Mengembalikan nilai dari key app token berupa String */
    public static String getAppToken(Context context)
    {
        return getSharedPreference(context).getString(APP_TOKEN,"");
    }




    /** Mengembalikan nilai nama aplikasi berupa string */
    public static String  getAppName()
    {
        return APP_NAME;
    }

    /** Mengembalikan nilai version berupa string */
    public static String  getAppProtocol()
    {
        return APP_PROTOCOL;
    }


    /** Mengembalikan nilai version berupa integer */
    public static Integer  getVersion()
    {
        return APP_VERSION;
    }


    /** Mengembalikan nilai maksimal delay volley */
    public static Integer  getVolleyMaxResponse()
    {
        return APP_VOLLEY_MAX_RESPONSE;
    }

    public static String  getMqttServer()
    {
        return MQTT_SERVER;
    }






    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key mqtt_server dengan parameter _token_id */
    public static void setMqttServer(Context context, String _server_url)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(MQTT_SERVER, _server_url);
        editor.apply();
    }

    /** Mengembalikan nilai dari key mqtt_server berupa String */
    public static String getMqttServer(Context context){
        return getSharedPreference(context).getString(MQTT_SERVER,"");
    }

    /** Deklarasi Edit Preferences dan menghapus data, sehingga menjadikannya bernilai default
     *  khusus data yang memiliki key KEY_USERNAME_SEDANG_LOGIN dan KEY_STATUS_SEDANG_LOGIN */
    public static void resetConfig(Context context)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(APP_TOKEN);
        editor.remove(REALNAME);
        editor.remove(DOMAIN);
        editor.remove(REGISTRATION_TOKEN);
        editor.remove(REGISTRATION_STATUS);
        editor.remove(MQTT_SERVER);
        editor.apply();
    }
}