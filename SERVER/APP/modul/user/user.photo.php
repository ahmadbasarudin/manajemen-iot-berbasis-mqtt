<?php
    /*
    |--------------------------------------------------------------------------
    | User Password 
    |--------------------------------------------------------------------------
    |Form untuk ganti password
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    $PAGE_ID = "USR113";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $JS_EXTENDED .= "
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    <link rel='stylesheet' href='modul/user/user.css'>
                    ";

    $s_form_input = "";
    if(isset($_REQUEST['user-id']))
    {
        $a_data_user = $oUser->getUserDetail($_REQUEST['user-id']);
        if(count($a_data_user)>0)
        {
            $s_form_input = "
                        <div class='card ml-auto mr-auto'>
                            <div class='container emp-profile'>
                                <form method='post' enctype='multipart/form-data' id='form-upload-photo'>
                                    <div class='row'>
                                        <div class='col-md-12 '>
                                            <div class='profile-img'>

                                                <img id='image_profile' src='{$a_data_user[0]['avatar']}' alt=''>
                                                <div class='file btn btn-lg btn-primary'>
                                                    Change Photo
                                                    <input name='file' id='file' type='file'>
                                                </div>
                                                <div id='progress-bar' class='progress-bar progress-bar-green' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 0%;height: 20px;'>
                                                          <span class='sr-only'>ass</span>
                                                </div>
                                                <input name='action' value='profilepicture' type='hidden'>
                                                <input name='user-id' value='{$a_data_user[0]['userID']}' id='user-id' type='hidden'>
                                            </div>
                                        </div>
                                    </div>
                                 </form>
                            </div>   
                        </div>";
        }
    }
    

                        
            
    $TITLE_MAIN = "Ganti Photo Profil";
    $TITLE_SUB = "";

    $BUTTON_ACTION = "
                                <button id='button-back' class='  btn btn-warning  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-caret-left'></i>
                                    </span>
                                    Kembali
                                </button>
    ";
    $CONTENT_MAIN = "

        <!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>
                        {$s_form_input}
                    </div>
                </div>
            </div>
        </div>


        <!-- END PAGE CONTENT -->
              ";
    $oUser->closeDB();
?>
