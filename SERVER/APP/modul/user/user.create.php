<?php
    /*
    |--------------------------------------------------------------------------
    | User Create 
    |--------------------------------------------------------------------------
    |Form untuk entry pengguna secara manual entry
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "USR104";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oUser = new UserInfo();
    $oWilayah = new Wilayah();
    $oGroup = new Group();


    $s_hak_akses_container = "";
    $a_hak_akses =  $oGroup->getList(" WHERE active=1 ","", "");

    if(isset($a_hak_akses))
    {
        if(count($a_hak_akses) > 0)
        {

            $s_hak_akses_container = "<div class='col-md-12'>
                                                <label for='hak_akses'>Hak Akses</label>
                                                <br />
                                                <br />
                                        <div class='row product-chooser '>
                                         ";
            for($i=0;$i<count($a_hak_akses);$i++)
            {
                $s_hak_akses_container .= "
                                        <div class='col-xs-12 col-sm-12 col-md-6 col-lg-3  '> 
                                            <div class='product-chooser-item   col-content    '>
                                                <div class='card-profile'>

                                                    <div class='card-header' style='background-image: url('assets/img/blogpost.jpg')'>
                                                        <div class='profile-picture'>
                                                            <div class='avatar avatar-xl'>
                                                                <img src='{$a_hak_akses[$i]['image']}'  class='avatar-img rounded-circle'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='card-body'>
                                                        <div class='user-profile text-center'>
                                                            <div class='name'>{$a_hak_akses[$i]['name']}</div>
                                                            <div class='desc'>{$a_hak_akses[$i]['notes']}</div>
                                                            <input type='radio' name='hak_akses' value='{$a_hak_akses[$i]['groupID']}' >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        ";
            }
            $s_hak_akses_container .= "</div>";
        }

    }




    $JS_EXTENDED .= "
                    <script src='assets/js/plugin/select2/js/select2.full.min.js'></script>


                    <!-- InputMask -->
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.date.extensions.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.extensions.js'></script>


                    <script src='assets/js/plugin/select2/js/select2.full.min.js'></script>

                    <script src='modul/user/user.js'></script>
                    ";
    $CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/js/plugin/select2/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/basar_component.css'>
                    <link rel='stylesheet' href='modul/user/user.css'>
                    ";
    $a_data_wilayah = $o_data_wilayah = $oWilayah->getList("","","");
    $s_wilayah ="";
    if(isset($a_data_wilayah))
    {
        $s_wilayah = "<select class='form-control select2' name='wilayah'  id='wilayah_umum' >";
        $s_wilayah .= "<option value=''>Silahkan Pilih Wilayah Kerja</option>";
        for($i=0;$i < count($a_data_wilayah);$i++)
        {
            $s_wilayah .= "<option value='{$a_data_wilayah[$i]['idWilayah']}'>{$a_data_wilayah[$i]['namaWilayah']}</option>";
        }
        $s_wilayah .= "</select>";
    }
    $s_form_input = "";

    $s_form_input = "
                    <form id='form-user-create' action='' method='post'>
                        <div >
                            <!-- /.box-header -->
                            <div class='box-body' >
        
                                {$s_hak_akses_container}
                                <div id='form-detail-user' >

                                    <div class='row'>
                                        <div class='col-md-6'>

                                            <div class='form-group'>
                                                <label for='password'>NIP</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                            <i class='fa fas fa-user'></i>
                                                        </span>
                                                    </div>
                                                    <input type='text' class='form-control' name='user-id'  >
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for='real-name'>Nama Lengkap</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                           <i class='fa fa-text-width'></i>
                                                        </span>
                                                    </div>
                                                   <input type='text' class='form-control' name='real-name'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for='realName'>Email</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                           <i class='fa fa-envelope'></i>
                                                        </span>
                                                    </div>
                                                   <input type='text' class='form-control'  name='mail'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for='realName'>Wilayah Kerja</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                           <i class='fa fa-home'></i>
                                                        </span>
                                                    </div>

                                                   $s_wilayah
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!-- /.row -->
                                    <div class='row'>
                                        
                                        <div class='col-md-12'>

                                            <div class='form-group'>
                                                <label for='realName'>Username</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                            <i class='fa fa-user'></i>
                                                        </span>
                                                    </div>
                                                    <input  name='username' class='form-control'  placeholder='Username'>
                                                </div>
                                            </div>
                                                
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for='realName'>Password</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                            <i class='fa fa-ellipsis-h'></i>
                                                        </span>
                                                    </div>
                                                    <input type='password' name='password' class='form-control'  placeholder='Password'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for='realName'>Ulangi Password</label>

                                                <div class='input-group mb-3'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                            <i class='fa fa-ellipsis-h'></i>
                                                        </span>
                                                    </div>
                                                    <input type='password' name='password-replay' class='form-control'  placeholder='Password'>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    <!-- /.row -->
                                </div>
                                
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";

                        
    $TITLE_MAIN = "Pengguna";
    $TITLE_SUB = "menambah pengguna";

    $BUTTON_ACTION = "
                                <button id='button-back' class='  btn btn-warning  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-caret-left'></i>
                                    </span>
                                    Kembali
                                </button>
                                <button id='button-create-user-apply' class='btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-plus'></i>
                                    </span>
                                    Tambah 
                                </button>
    ";

                        
    $CONTENT_MAIN = "

    <!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Form Wilayah Kerja</div>
                                </div>
                                <div class='card-body'>
                                    <div >
                                        {$s_form_input}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
    $oUser->closeDB();
    $oWilayah->closeDB();
    $oGroup->closeDB();
?>