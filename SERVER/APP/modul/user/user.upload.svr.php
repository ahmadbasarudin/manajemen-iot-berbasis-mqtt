<?php

 
    $PAGE_ID = "USR111";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    $folder_docs = str_replace("upload.svr.php", "", $_SERVER['SCRIPT_FILENAME']);
    $folder_upload = $folder_docs."uploaded/";
    //print_r($folder_upload);
    $response =  array('status'=>'error','desc'=>'not responding');
    if($_REQUEST['action'] == "profilepicture")
    {
        $uploadedFile = '';
        if(!empty($_FILES["file"]["type"])){
            $fileName = time().'_'.$_FILES['file']['name'];
            $valid_extensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
            if((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                $sourcePath = $_FILES['file']['tmp_name'];
                $targetPath = $folder_upload.$fileName;
                if(move_uploaded_file($sourcePath,$targetPath)){
                    $uploadedFile = $fileName;
                    $response['status'] ="success";
                    $response['desc'] ="file sudah diupload";
                    $response['filename'] = $fileName; 
                }
                else
                {
                    $response['status'] ="error";
                    $response['desc'] ="file tidak dapat diupload";
                }
            }
            else
            {
                $response['status'] ="error";
                $response['desc'] ="file harus berupa gambar";
            }
        }
        else
        {
            $response['status'] ="error";
            $response['desc'] ="file tidak bertipe";
        }
    }
    echo json_encode($response);
?>