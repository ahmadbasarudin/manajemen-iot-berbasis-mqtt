<?php
    /*
    |--------------------------------------------------------------------------
    | User Password 
    |--------------------------------------------------------------------------
    |Form untuk ganti password
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    $PAGE_ID = "USR112";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $JS_EXTENDED .= "
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-ganti-password-apply' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-user'></i> Ganti
                        </button>
                    ";
    $s_form_input = "";

    $s_form_input = "
                        <form id='form-ganti-password' action='' method='post'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <div class='input-group mb-3'>
                                                <div class='input-group-prepend'>
                                                    <span class='input-group-text' >
                                                        <i class='fa fas fa-key'></i>
                                                    </span>
                                                </div>
                                                <input type='password' name='password_lama' class='form-control'  placeholder='Password Lama'>
                                            </div>
                                    </div>
                                    <div class='col-md-12'>
                                            <div class='input-group mb-3'>
                                                <div class='input-group-prepend'>
                                                    <span class='input-group-text' >
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </span>
                                                </div>
                                                <input type='password' name='password_baru' class='form-control'  placeholder='Password Baru'>
                                            </div>
                                    </div>
                                    <div class='col-md-12'>
                                            <div class='input-group mb-3'>
                                                <div class='input-group-prepend'>
                                                    <span class='input-group-text' >
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </span>
                                                </div>
                                                <input type='password' name='password_ulang' class='form-control'  placeholder='Password Baru'>
                                            </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                                <!-- /.row -->
                        </form>";
                        
            
    $TITLE_MAIN = "Pergantian Password";
    $TITLE_SUB = "";

    $BUTTON_ACTION = "
                                <button  id='button-ganti-password-apply'  class='button-create-user  btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-key'></i>
                                    </span>
                                    Ganti Password
                                </button>
    ";
    $CONTENT_MAIN = "

        <!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-body'>
                                    <div >
                                        {$s_form_input}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- END PAGE CONTENT -->
              ";
    $oUser->closeDB();
?>
