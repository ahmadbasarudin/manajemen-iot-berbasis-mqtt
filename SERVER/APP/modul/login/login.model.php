<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	$PAGE_ID="LOGIN";
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.client.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.log.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.firewall.php");
	$oUser = new UserInfo();
	$oLog = new Log();
	$oClient = new Client();
	//jika variable error sudah terisi maka kosongkanlah
	$respone['status'] = "error";
	$respone['desc'] = "";

	$s_error  = "";

	if(isset($_REQUEST['TOKEN_KEY']))
	{
		if($_REQUEST['TOKEN_KEY'] != "")
		{
			$s_condition = " WHERE loginToken = '{$_REQUEST['TOKEN_KEY']}' AND active =1; ";

			if($oUser->getUserCount($s_condition)> 0)
			{
				/**
				* dialihkan ke halaman yang telah disecure
				*/
				$a_userinfo = $oUser->getUserList($s_condition,"","");
	      
				/*
				|$CLIENT_ADDRESS didapat dari modul firewall
				*/
				if($oLog->logging($a_userinfo[0]['userID'],$oClient->getUserAgent(),$CLIENT_ADDRESS,$oClient->getClientUrl(),$PAGE_ID,"telah berhasil login"))
				{
					$respone['status'] = "success";
					$respone['desc']  = "Login berhasil";
					$respone['token_key'] = session_id();
				}
				else
				{
					$respone['status'] = "error";
					$respone['desc']  = "Autentifikasi gagal";
				}
			}
			//jika username dan password tidak ditemukan disystem
			else
			{
				$respone['status'] = "error";
				$respone['desc']  = "TOKEN KEY gagal diidentifikasi";

			}
		}
		else
		{	
			$respone['status'] = "error";
			$respone['desc']  = "TOKEN KEY tidak boleh kosong";

		}
			

	}
	elseif(LOGIN_REFERENCE == "PASSWORD")
	{
		if( (!isset($_REQUEST['username']) ) || ($_REQUEST['username'] == "") )
		{
			$respone['status'] = "error";
			$aErrors[] = "Username harus diisi";
		}
		if( (!isset($_REQUEST['password']) ) || ($_REQUEST['password'] == "") )
		{
			$respone['status'] = "error";
			$aErrors[] = "Password harus diisi";
		}
		//periksa username apakah ada di dalam database
		if(isset($_REQUEST['username']) && isset($_REQUEST['password']) &&($_REQUEST['username'] != "") && ($_REQUEST['password'] != "") )
		{
			//validasi login ke system
			//jika username dan password ditemukan disystem
			$USERNAME = $oUser->antiInjection($_REQUEST['username']);
			$PASSWORD = $oUser->antiInjection($_REQUEST['password']);
			$s_condition  = " WHERE username= '".$_REQUEST['username']."' ";
			if($oUser->getUserCount($s_condition)<1)
			{
				$respone['status'] = "error";
				$respone['desc']  = "username tidak ditemukan";
			}else{
				$a_username =$oUser->getUserList($s_condition,"","");
				if($a_username[0]['active'] == 2)
				{
					$respone['status'] = "error";
					$respone['desc']  = "username telah dinonaktifkan";
				}
				elseif($a_username[0]['active'] == 0)
				{
					$respone['status'] = "error";
					$respone['desc']  = "username telah dihapus";
				}
				elseif($a_username[0]['active'] == 1)
				{
					$s_condition  = " WHERE username= '{$USERNAME}' AND password=md5('{$PASSWORD}') AND active=1 ";
					if($oUser->getUserCount($s_condition)> 0)
					{
						if($oUser->updateSessionLogin($USERNAME))
						{
	                        $_SESSION['LoggedIn'] = true;
	                        $_SESSION['username'] = $USERNAME;
	                        $a_userinfo = $oUser->getUserIDByUsername($USERNAME);

							if($oLog->logging($a_userinfo[0]['userID'],$oClient->getUserAgent(),$CLIENT_ADDRESS,$oClient->getClientUrl(),$PAGE_ID,"telah berhasil login"))
							{
								$respone['status'] = "success";
								$respone['desc']  = "Login berhasil";
							}
							else
							{
								$respone['status'] = "error";
								$respone['desc']  = "Autentifikasi gagal di log";
							}

						}
						else
						{
							$respone['status'] = "error";
							$respone['desc']  = "sesi login error";
						}

					}
					else
					{
						$respone['status'] = "error";
						$respone['desc']  = "password salah";
					}
				}
				else
				{
					$respone['status'] = "error";
					$respone['desc']  = "status username tidak diketahui";
				}
			}
		}
		else{
		   	foreach ($aErrors as $error) {
		        $s_error .= "$error  <br />";
		   	}
	  	 	$respone['desc']  = $s_error;
		}
	}

	echo json_encode($respone);
	$oUser->closeDB();
	$oLog->closeDB();
?>