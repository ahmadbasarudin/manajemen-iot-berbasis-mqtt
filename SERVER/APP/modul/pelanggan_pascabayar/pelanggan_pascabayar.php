<?php
    /*
    |--------------------------------------------------------------------------
    | pelanggan pascabayar
    |--------------------------------------------------------------------------
    |Controler  modul pelanggan pascabayar
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "PPB100";
    $MODUL = "pelanggan_pascabayar";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    if(isset($_REQUEST['type']))
    {
        //direk json
        if ($_REQUEST['type'] == "model") 
        {
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.model.php");
        } 
        //selain direk langsung
        elseif($_REQUEST['type'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.view.php");
            include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.view.php");
        include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
    }

?>