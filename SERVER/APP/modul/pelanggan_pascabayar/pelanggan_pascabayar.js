$(document).ready(function() {

    var idpel = [];
    $('.idpel').each(function() {
        idpel.push($(this).text()); 
    });
    i=0;
    jQuery('#button-crosscheck-tagihan').click(function() {
        if($('.idpel').length)
        {

            getTagihanPelanggan(idpel[i]);
            $(".progress-card").show();
            //console.log(idpel);
        }
        else
        {
            Swal.fire({
                type: 'error',
                title: '',
                html: 'Tidak ada pelanggan yang akan diperiksa',
                onOpen: () => {
                    var zippi = new Audio('assets/sound/error.mp3')
                    zippi.play();
                }
            });
        }

    });


    function updateProgressBar()
    {   
                var position = i / ($('.idpel').length-1);
                var percent = Math.round(position * 100) + '%';


                //$("#label-idpel").html(idpel[i] + " || " + i);
                $("#label-percent").html(percent);
                $("#progressbar").css({ width: percent});
    }

    function getTagihanPelanggan(_idpel)
    {
        $.ajax({
                    type: 'POST',
                    url: "index.php?page=pelangganpascabayar&type=model&action=check&idpel=" + _idpel,
                    dataType:"json",
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                            updateProgressBar();
                            i++;
                            if (typeof idpel[i] !== 'undefined') {
                              // your code here

                              getTagihanPelanggan(idpel[i]);
                            }
                            return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            var icon_status = "";
                            if(msg.status == 'success')
                            {
                                icon_status = "<i class=' text-success far fa-check-circle'></i>";
                                var zippi = new Audio('assets/sound/success.mp3')
                                zippi.play();

                            }
                            else
                            {
                                icon_status = "<i class=' text-danger far fa-times-circle'></i>";
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }

                            $($('.check-status')[i]).html(    " " 
                                                            + icon_status 
                                                            + msg.status 
                                                            + " "
                                                            + msg.desc
                                                            );

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            Swal.fire({
                                type: 'error',
                                title: '',
                                text: 'Terjadi error saat proses kirim pelanggan : ' +  idpel[i],
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });

                            /*
                            i++;
                            if (typeof idpel[i] !== 'undefined') {
                              // your code here
                              getTagihanPelanggan(idpel[i])
                            }
                            */
                        }
                });//end of ajax
    }

});