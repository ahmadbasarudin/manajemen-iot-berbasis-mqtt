<?php
    /*
    |--------------------------------------------------------------------------
    | pelanggan pascabayar list
    |--------------------------------------------------------------------------
    |list  modul pelanggan pascabayar
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.device_iot.php");
    $oDeviceIot = new DeviceIot();
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    $oGroup = new Group();


    $a_title[] = "ID PERANGKAT";
    $a_title[] = "ID PELANGGAN";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " ";

    $s_table_container = "";
    $s_condition = " WHERE diStatus =1 AND LENGTH(diPelangganPasca) = 12   ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `diDateCreate` DESC ";


    $JS_EXTENDED .= "
                    <script src='assets/js/plugin/datatables/datatables.min.js'></script>
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";


    $a_data = $oDeviceIot->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-device' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
        $s_table_container .="</tr>
                        </thead>";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {
            $s_status = "";
            $s_button = "";
            $s_bg_column = "";


            $s_button .= "<a class='button-customer dropdown-item' href='#' record-id='{$a_data[$i]['diID']}' >Ganti Pembeli</a>
                                    ";
            $s_button .= "<a class='button-delete dropdown-item' href='#' record-id='{$a_data[$i]['diID']}' >Hapus</a>
                                    ";
            //$s_button = "<button class='button-delete btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['diID']}'  style='margin-left: 2px;'>Hapus</button>";


                    
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['diID'])."<br>"
                                ."</td>";
            $s_table_container .= "<td  align='left' >"
                                     ."<span class='idpel'>".strtoupper ($a_data[$i]['diPelangganPasca'])."</span> <span class='check-status'></span>"
                                ."</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }



    $TITLE_MAIN = "Device IoT";
    $TITLE_SUB = "";


    $BUTTON_ACTION = "
                                <button id='button-crosscheck-tagihan' class=' btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fas fa fa-exchange-alt'></i>
                                    </span>
                                    Kroscek Tagihan
                                </button>
    ";
    $CONTENT_MAIN = "
        <!-- BEGIN PAGE CONTENT -->
        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div id='progress-card'  class='progress-card' style='display: none;'>
                                <div class='progress-status'>
                                    <span id='label-idpel' class='text-muted'>..</span>
                                    <span id='label-percent' class='text-muted fw-bold' > 0%</span>
                                </div>
                                <div class='progress'>
                                    <div id='progressbar' class='progress-bar progress-bar-striped bg-warning' role='progressbar' style='width: 0%' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' data-toggle='tooltip' data-placement='top' title='' data-original-title='0%'></div>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-body'>
                                    <div class='table-responsive'>
                                        {$s_table_container}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
              ";
    $oDeviceIot->closeDB();
    $oGroup->closeDB();

?>