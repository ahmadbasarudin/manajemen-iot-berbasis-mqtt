<?php
/*
|--------------------------------------------------------------------------
| pelanggan pascabayar model
|--------------------------------------------------------------------------
|
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
    $PAGE_MODEL = true; // khusus untuk halaman model
    
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.digiflazz.php");
    $oDigiflazz = new Digiflazz();
	//jika variable error sudah terisi maka kosongkanlah
	$response['status'] = "error";
	$response['desc'] = "";

	$s_error  = "";
    $a_errors = array();
    if(isset($_REQUEST['action']))
    {
        
        if($_REQUEST['action'] == 'check')
        {
            if(!isset($_REQUEST['idpel']))
            {
                $a_errors[] = "id pelanggan tidak ada";
            }
            elseif($_REQUEST['idpel'] == "")
            {
                $a_errors[] = "id pelanggan masih kosong";
            }
            elseif(strlen($_REQUEST['idpel']) != 12 )
            { 
                $a_errors[] = "id pelanggan harus 12 digit";
            }
            if (!$a_errors) 
            {
                //check api 

                $transaksi_id = date("His");
                $idpel= $_REQUEST['idpel'];
                $sku_code = DIGIFLAZZ_SKU_PLN;

                $result_api  =  $oDigiflazz->getStatusPLN($transaksi_id,$idpel,$sku_code);

                $json_data = json_decode($result_api, true);
                //check bila format data json sudah benar
                if (json_last_error() === JSON_ERROR_NONE) {
                    // JSON is valid

                    // tagihan sudah dibayar
                    if($json_data['data']['rc'] == '60')
                    {
                        $response['status'] = 'success';
                        $response['desc'] = "tagihan sudah dibayar";
                        $response['device'] = 'ON';
                    }
                    elseif($json_data['data']['rc'] == '00')
                    {

                        $response['status'] = 'error';
                        $response['desc']   = "tagihan belum dibayar." 
                                            . "tagihan : ".$json_data['data']['price'] ."."
                                            . "lembar : ".$json_data['data']['desc']['lembar_tagihan'] ."."
                                            . "atas nama : ".$json_data['data']['customer_name'] ;
                        $response['device'] = 'OFF';
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['desc'] = "status tagihan tidak ditahui";
                    }
                    $response['data'] =$json_data['data'];


                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "komunikasi dengan server pelanggan error";

                }

                /*
                if($oDeviceIot->delete($_REQUEST['diID']))
                {

                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "device gagal dihapus";
                }
                */
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                    $s_error .= "$error<br />";
                }
                $response['status'] = 'error';
                $response['desc'] = $s_error;
            }
        }


        if ($a_errors) 
        {
            $s_error =  '';
            foreach ($a_errors as $error) 
            {
                $s_error .= "$error<br />";
            }
            $response['status'] = 'error';
            $response['desc'] = $s_error;
        }
    }
    sleep(1);
	echo json_encode($response);
	$oDigiflazz->closeDB();

?>