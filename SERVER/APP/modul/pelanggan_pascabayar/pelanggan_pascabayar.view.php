<?php
    /*
    |--------------------------------------------------------------------------
    | pelanggan pascabayar view
    |--------------------------------------------------------------------------
    |view  modul pelanggan pascabayar register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    
    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
        } 
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.create.php");
        }

    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
    }

?>