$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-COMPUTER
    **************************************************/
    if($('#table-wilayah').length)
    {
        jQuery('.button-wilayah-create').click(function() {
            document.location='index.php?page=wilayah&action=create'
        });
        jQuery('.button-wilayah-simplemode').click(function() {
            document.location='index.php?page=wilayah&action=simplemode'
        });

        $('#table-wilayah').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 0 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[10, 25, -1], [10, 25, "All"]],
             order: [[ 0, "asc" ]]
        });


        $('#table-wilayah tbody').on( 'click', '.button-wilayah-delete', function () 
        {
            wilayah_id = $(this).attr('record-id');
            wilayah_name = $(this).parent().parent().find(".wilayah-name").html();  
            Swal.fire({
                    title: 'Konfirmasi',
                    html: "Apakah  yakin  wilayah kerja <b>"+wilayah_name +"</b> dihapus?",
                    type: 'warning',
                    onOpen: () => {
                        var zippi = new Audio('assets/sound/warning.mp3')
                        zippi.play();
                    },
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, dihapus saja',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) 
                    {

                        $.ajax({
                            type: 'POST',
                            url: "index.php?page=wilayah&type=model&action=delete",
                            dataType:"json",
                            data: 'wilayah_id='+wilayah_id,
                            cache:false,
                            //jika complete maka
                            complete:
                                function(data,status)
                                {
                                 return false;
                                },
                            success:
                                function(msg,status)
                                {
                                    //alert(msg);
                                    $('#notification-container').hide();
                                    if(msg.status == 'success')
                                    {


                                        Swal.fire({
                                            type: 'success',
                                            title: 'Success...',
                                            text: msg.desc,
                                            timer: 1200,
                                            onOpen: () => {
                                                var zippi = new Audio('assets/sound/success.mp3')
                                                zippi.play();
                                            },
                                            onClose: () => {
                                                document.location='index.php?page=wilayah&action=list'
                                            }
                                        })


                                    }
                                    else
                                    {
                                        $('#notification-container').removeClass('alert-success');
                                        $('#notification-container').addClass('alert-danger'); 
                                        $('#notification-content').html(msg.desc);
                                        $('#notification-container').show(100); 
                                    }
                                    $('html, body').animate({
                                    scrollTop: $('#container-alert').offset().top
                                    }, 500);

                                },
                            //untuk sementara tidak digunakan
                            error:
                                function(msg,textStatus, errorThrown)
                                {
                                    $('#notification-container').hide();
                                    $('#notification-container').removeClass('alert-success');
                                    $('#notification-container').addClass('alert-danger'); 
                                    $('#notification-content').html('Terjadi kegagalan proses transaksi');
                                    $('#notification-container').show(); 
                                    $('html, body').animate({
                                    scrollTop: $('#container-alert').offset().top
                                    }, 500);
                                }
                        });//end of ajax
                    }
            });


            //alert(wilayah_id);
            //alert(device_register_id);
        });


    }
    /*   END TABLE-WILAYAH
    **************************************************/
        
    if($('#button-back').length)
    {
        jQuery('#button-back').click(function() {
            document.location='index.php?page=wilayah&action=list'
        });
    }
    if($('#button-wilayah-create-apply').length)
    {
        jQuery('#button-back').click(function() {
            document.location='index.php?page=wilayah&action=list'
        });
        $('#button-wilayah-create-apply').click(function() 
        {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=wilayah&type=model&action=create",
                    dataType:"json",
                    data: $("#form-create").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.status == 'success')
                            {


                                Swal.fire({
                                    type: 'success',
                                    title: '',
                                    text: msg.desc,
                                    timer: 1200,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/success.mp3')
                                        zippi.play();
                                    },
                                    onClose: () => {
                                        document.location='index.php?page=wilayah&action=list';
                                    }
                                });

                            }
                            else
                            {
                                
                                Swal.fire({
                                    type: 'error',
                                    title: '',
                                    html: msg.desc,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/error.mp3')
                                        zippi.play();
                                    }
                                });
                            }

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            
                            Swal.fire({
                                type: 'error',
                                title: '',
                                text: 'Terjadi error saat proses kirim data',
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
                });//end of ajax

        });
    }

    if($('.select2').length)
    {
        $('.select2').select2();
    }
    

});