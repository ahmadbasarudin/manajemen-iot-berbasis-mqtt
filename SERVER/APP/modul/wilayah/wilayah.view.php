<?php
    /*
    |--------------------------------------------------------------------------
    | {$MODUL}
    |--------------------------------------------------------------------------
    |view  modul user
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
        } 
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.create.php");
        }
        elseif($_REQUEST['action'] == "simplemode")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.simplemode.php");
        }
        elseif($_REQUEST['action'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
    }

?>