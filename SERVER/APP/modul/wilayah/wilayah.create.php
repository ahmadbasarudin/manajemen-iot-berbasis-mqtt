<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah Create 
    |--------------------------------------------------------------------------
    |Form untuk entry wilayah
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();

    $JS_EXTENDED .= "
                    <script src='assets/js/plugin/select2/js/select2.full.min.js'></script>
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/js/plugin/select2/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/basar_component.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-wilayah-create-apply' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus'></i> Tambah
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";

    $a_data_list_wilayah = $oWilayah->getList(" WHERE TRUE ","","");

    $s_wilayah = "<select class='form-control select2' name='parent_wilayah'  placeholder='Wilayah Induk' >";
                $s_wilayah .= "<option value=''>tidak ada induk </option>";
                for($i=0;$i < count($a_data_list_wilayah);$i++)
                {
                    $s_wilayah .= "<option value='{$a_data_list_wilayah[$i]['idWilayah']}'>{$a_data_list_wilayah[$i]['namaWilayah']}</option>";
                }
                $s_wilayah .= "</select>";

    $s_form_input = "
                    <form id='form-create' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                <div class='row'>
                                    <div class='col-md-12'>

                                        <div class='input-group mb-3'>
                                            <div class='input-group'>
                                                <label>Wilayah Induk:</label>
                                            </div>
                                            <div class='input-group-prepend'>
                                                <span class='input-group-text' >
                                                    <i class='fa fas fa-home'></i>
                                                </span>
                                            </div>
                                            $s_wilayah
                                        </div>
                                           
                                    </div>

                                    <div class='col-md-4'>
                                        <div class='input-group mb-3'>
                                            <div class='input-group-prepend'>
                                                <span class='input-group-text' >
                                                    <i class='fa fas fa-key'></i>
                                                </span>
                                            </div>
                                                <input type='text' class='form-control' name='kode_wilayah' placeholder='kode wilayah kerja'>
                                        </div>
                                            
                                    </div>
                                    <div class='col-md-8'>
                                        <div class='input-group mb-3'>
                                            <div class='input-group-prepend'>
                                                <span class='input-group-text' >
                                                    <i class='fa fa-text-width'></i>
                                                </span>
                                            </div>
                                            <input type='text' class='form-control'  name='nama_wilayah' placeholder='Nama wilayah kerja'>
                                        </div>
                                            
                                    </div>
                                    
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
    $TITLE_MAIN = "Wilayah Kerja";
    $TITLE_SUB = "menambah wilayah kerja";

    $BUTTON_ACTION = "
                                <button id='button-back' class='  btn btn-warning  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-caret-left'></i>
                                    </span>
                                    Kembali
                                </button>
                                <button id='button-wilayah-create-apply' class='btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-plus'></i>
                                    </span>
                                    Tambah 
                                </button>
    ";

            
    $CONTENT_MAIN = "

    <!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card bg-warning text-white'>
                                <div class='card-body'>
                                    <div >
                                        <h4> Petunjuk Pengisian</h4>
                                        <b>kode wilayah</b> parent merupakan induk dari wilayah yang akan diisi. dibiarkan kosong jika wilayah yang akan diisi merupakan wilayah induk<br />
                                        <b>kode wilayah</b> diisi dengan 6 angka  berdasarkan kode dari masing masing wilayah.<br />
                                        <b>nama wilayah</b> diisi dengan alpanumerik yang merupakan nama dari wilayah tersebut.<br />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Form Wilayah Kerja</div>
                                </div>
                                <div class='card-body'>
                                    <div >
                                        {$s_form_input}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
    $oWilayah->closeDB();

?>