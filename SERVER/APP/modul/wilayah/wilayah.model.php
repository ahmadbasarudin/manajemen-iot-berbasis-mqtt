<?PHP
     /**
      * wilayah.model.php
      * 
      * File ini digunakan untuk model wilayah
      *  
      *
      *
      * prefix parameter:
      *    n  - node
      *    o  - object
      *    a  - array
      *    s  - string
      *    b  - boolean
      *    f  - float
      *    i  - integer
      *    uk  - unknown
      *    fn - function
      *    _  - parameter
      **/
    $PAGE_ID = "WIL100";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();

    require_once($SYSTEM['DIR_PATH']."/class/function.notification.local.php");

    $a_errors = array();
    $respone['status'] = 'error';
    $respone['desc'] = "tidak diproses";


     
    if(isset($_REQUEST['action']))
    {

        if($_REQUEST['action'] == 'delete')
        {
            if($_REQUEST['wilayah_id'] == "")
            {
                $a_errors[] = "wilayah id tidak ada";
            }
            if (!$a_errors) 
            {


                $s_condition = " WHERE parentWilayah = '{$_REQUEST['wilayah_id']}'; " ;
                if($oWilayah->getCount($s_condition)==0)
                {

                    if($oWilayah->delete($_REQUEST['wilayah_id']))
                    {

                        $respone['status'] = 'success';
                        $respone['desc'] = "wilayah  berhasil dihapus.";
                        $s_notif_message = "{$USER[0]['realName']} menghapus wilayah  <span class='label label-danger'>{$_REQUEST['wilayah_id']}";  
                        create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                                          
                    }
                    else
                    {

                        $respone['status'] = 'error';
                        $respone['desc'] = "wilayah tidak dapat dihapus.";
                    }
                }
                else
                {

                    $respone['status'] = 'error';
                    $respone['desc'] = "wilayah tidak dapat dihapus karena masih memiliki wilayah dibawahnya. untuk menghapus wilayah ini, anda harus menghapus wilayah yang berada dibawahnya terlebih dahulu.";
                }

            }

        }
        elseif($_REQUEST['action'] == 'create')
        {
            if(!isset($_REQUEST['kode_wilayah']))
            {
                $a_errors[] = "wilayah id tidak ada";
            }
            else
            {
                if($_REQUEST['kode_wilayah'] == "")
                {
                    $a_errors[] = "kode wilayah harus diisi";
                }
            }
            if(!isset($_REQUEST['nama_wilayah']))
            {
                $a_errors[] = "nama wilayah  tidak ada";
            }
            else
            {
                if($_REQUEST['nama_wilayah'] == "")
                {
                    $a_errors[] = "nama wilayah harus diisi";
                }
            }
            if (!$a_errors) 
            {
                $_REQUEST['kode_wilayah'] = $oWilayah->antiInjection($_REQUEST['kode_wilayah']);
                //jika kode pernah dimasukan sebelumnya
                if($oWilayah->getCount(" WHERE idWilayah = '{$_REQUEST['kode_wilayah']}'") > 0)
                {

                    $respone['status'] = 'error';
                    $respone['desc'] = "kode wilayah sudah pernah dimasukan sebelumnya";
                }
                else
                {
                    $a_wilayah['idWilayah'] = $_REQUEST['kode_wilayah'];
                    $a_wilayah['namaWilayah'] = $_REQUEST['nama_wilayah'];
                    $a_wilayah['parentWilayah'] = $_REQUEST['parent_wilayah'];
                    
                    if($oWilayah->create($a_wilayah))
                    {

                        $respone['status'] = 'success';
                        $respone['desc'] = "data wilayah berhasil dimasukan";
                        $s_notif_message = "{$USER[0]['realName']} menambah wilayah  <span class='label label-success'>{$a_wilayah['namaWilayah']}";  
                        create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                        
                    }
                    else
                    {
                        $respone['status'] = 'error';
                        $respone['desc'] = "sistem tidak dapat memasukan data wilayah";

                    }
                }
            }
        }

        if ($a_errors) 
        {
            $s_error =  '';
            foreach ($a_errors as $error) 
            {
                $s_error .= "$error<br />";
            }
            $respone['status'] = 'error';
            $respone['desc'] = $s_error;
        }


          
    }
    echo json_encode($respone);
    $oWilayah->closeDB();

?>