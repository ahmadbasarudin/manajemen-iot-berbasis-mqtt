<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah view
    |--------------------------------------------------------------------------
    |view  modul wilayah 
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $oWilayah = new Wilayah();

    $s_table_container = "";
    $s_condition = " WHERE child.parentWilayah is NULL ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `idWilayah` ASC ";


    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/wilayah/wilayah.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";


    $a_data_parent = $oWilayah->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data_parent))
    {
        for($i=0;$i<count($a_data_parent);$i++)
        {


            $a_data_child = $oWilayah->getList(" WHERE child.parentWilayah = '{$a_data_parent[$i]['idWilayah']}' ", " ORDER BY child.idWilayah ", $s_limit);
            $s_child_container = "";
            if(isset($a_data_child))
            {
                for($j=0;$j<count($a_data_child);$j++)
                {
                    $s_child_container .= "<span class='label label-warning'>{$a_data_child[$j]['idWilayah']} {$a_data_child[$j]['namaWilayah']}</span> ";
                }
            }
            $s_table_container .= "
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='alert bg-gray disabled color-palette'>
                                            <div class='alert alert-success alert-dismissible'>{$a_data_parent[$i]['idWilayah']} {$a_data_parent[$i]['namaWilayah']}</div>
                                            {$s_child_container}
                                        </div>
                                    </div>
                                </div>
                                ";
        }
          
        
    }
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>WILAYAH KERJA</h4>
                                </div>
                                <div style='float:right'>


                                    <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                                        <i class='fa fa-caret-left'></i> kembali
                                    </button>
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oWilayah->closeDB();

?>