<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
    $PAGE_MODEL = true; // khusus untuk halaman model
    
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.client.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.log.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.firewall.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.firebase.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
	$oUser = new UserInfo();
	$oLog = new Log();
	$oClient = new Client();
	$oDevice = new Device();
    $oNotif = new Notification();
	//jika variable error sudah terisi maka kosongkanlah
	$respone['status'] = "error";
	$respone['desc'] = "";

	$s_error  = "";
    $a_errors = array();
    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == 'aktifasi')
        {
            $PAGE_ID = "UDR111";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

            if($_REQUEST['durID'] == "")
            {
                $a_errors[] = "device register ID tidak ada";
            }
            if (!$a_errors) 
            {
            	//update di datauser set 1= aktif, 0 = disable
            	if($oDevice->activationDeviceRegister($_REQUEST['durID'],"1"))
            	{
                    $message = "perangkat anda telah diaktifkan oleh ".$USER[0]['realName'];
                    //kirim pesan 
                    $a_data = $oDevice->getList(" WHERE durID = '{$_REQUEST['durID']}' ","","");
                    if($oNotif->create(

                        "register device" ,
                        $message ,
                        "" ,
                        $a_data[0]['userID'] ,
                        $a_data[0]['firebaseToken'] ,
                        $USER[0]['userID'] ,
                        ""
                    ))
                    {

                        $respone['status'] = 'success';
                        $respone['desc'] = "perangkat  berhasil diaktifkan";


                        $a_firebase_data[0]['notif-to-firebase-id'] = $a_data[0]['firebaseToken']; 
                        $a_firebase_data[0]['notif-id'] = $oNotif->getCount("");
                        $a_firebase_data[0]['notif-body'] = $message;  
                        $a_firebase_data[0]['notif-title'] = "PERANGKAT AKTIF";
                        

                        $a_checksum['total']= 0;
                        $a_checksum['error'] = 0;
                        $a_checksum['success'] = 0;
                        cron_send_notification($a_firebase_data,$SYSTEM,0,$a_checksum);
                    }
                    else
                    {
                        $respone['status'] = 'error';
                        $respone['desc'] = "ada error waktu notifikasi";
                    }

            	}
            	else
            	{
                    $respone['status'] = 'error';
                    $respone['desc'] = "device gagal diregister";
            	}
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
        }
        elseif($_REQUEST['action'] == 'testnotif')
        {
            $PAGE_ID = "UDR112";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

            if($_REQUEST['device-id'] == "")
            {
                $a_errors[] = "device register ID tidak ada";
            }
            if (!$a_errors) 
            {
                //echo $_REQUEST['device-id'];
                $a_data = $oDevice->getList(" WHERE durID = '{$_REQUEST['device-id']}' ","","");
                if(count($a_data)>=1)
                {

                    $a_firebase_data[0]['notif-to-firebase-id'] = $a_data[0]['firebaseToken']; 
                    $a_firebase_data[0]['notif-id'] = $oNotif->getCount("");
                    $a_firebase_data[0]['notif-body'] = "test notifikasi ".$SYSTEM['APP_NAME']; 
                    $a_firebase_data[0]['notif-title'] = "UJI PESAN";
                    

                    $a_checksum['total']= 0;
                    $a_checksum['error'] = 0;
                    $a_checksum['success'] = 0;
                    $a_result = cron_send_notification($a_firebase_data,$SYSTEM,0,$a_checksum);
                    //$respone['data'] = $a_result;
                    if($a_result['success'] > 0)
                    {
                        $respone['status'] = 'success';
                        $respone['desc'] = "pesan  di device {$a_data[0]['deviceModel']} sudah terkirim ";
                    }
                    else
                    {
                        $a_error_desc = json_decode($a_result['result'][0]);

                        $respone['status'] = 'error';
                        $respone['desc'] = "gagal dalam mengirim pesan. firebase : <b>". $a_error_desc->results[0]->error."</b>";
                    }
                        
                }
                else
                {

                    $respone['status'] = 'error';
                    $respone['desc'] = "device tidak ditemukan";
                }
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
        }
        elseif($_REQUEST['action'] == 'delete')
        {
            $PAGE_ID = "UDR113";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            
            if($_REQUEST['durID'] == "")
            {
                $a_errors[] = "device register ID tidak ada";
            }
            if (!$a_errors) 
            {
                //update di datauser
                if($oDevice->delete($_REQUEST['durID']))
                {
                    $respone['status'] = 'success';
                    $respone['desc'] = "device berhasil dihapus";

                }
                else
                {

                    $respone['status'] = 'error';
                    $respone['desc'] = "device gagal dihapus";
                }
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
        }
    }
	echo json_encode($respone);
	$oUser->closeDB();
	$oLog->closeDB();
	$oDevice->closeDB();
    $oNotif->closeDB();

?>