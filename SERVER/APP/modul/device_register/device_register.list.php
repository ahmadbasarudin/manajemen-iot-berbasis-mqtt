<?php
    /*
    |--------------------------------------------------------------------------
    | device list
    |--------------------------------------------------------------------------
    |list  modul device register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    $PAGE_ID = "UDR103";

    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
    $oDevice = new Device();
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    $oGroup = new Group();

    /**
    *   configuration variable
    **/
    $s_table_tile   = "Pengguna Aplikasi Mobile";

    

    $a_title[]      = "PENGGUNA";
    $a_title[]      = "PERANGKAT";
    $a_title[]      = "TANGGAL DAFTAR";
    $a_title[]      = "";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:10px;' ";

    $s_table_container = "";
    $s_condition = " WHERE registerStatus !=2 ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `dateRegister` DESC ";


    $JS_EXTENDED .= "
                    <script src='assets/js/plugin/datatables/datatables.min.js'></script>
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";


    //$GROUP didapat dari /modul/core/init,php
    if($oGroup->isGroup($USER[0]['userID'],$GROUP['ADMIN']))
    {
        $s_condition = " WHERE registerStatus !=9   ";       
    }

    $a_data = $oDevice->getList($s_condition, $s_order, $s_limit);

    if(isset($a_data) && is_array($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-device' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
        $s_table_container .="</tr>
                        </thead>";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {
            $s_status = "";
            $s_button = "";
            $s_bg_column = "";

            //------------- begin button-------------------
            if($a_data[$i]['registerStatus'] == '0')
            {
                $s_button = "<button class='button-device-activation btn btn-flat   btn-success btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Aktifasi</button>";
            }
            elseif($a_data[$i]['registerStatus'] == '1')
            {
                if($a_data[$i]['firebaseToken'] == $a_data[$i]['mainFirebase'] )
                {
                    $s_status = "  <span class='label label-primary'>device utama</span> ";
                    $s_button = "<button class='button-send-message btn btn-flat   btn-warning btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Uji Pesan</button>";
                }
                else
                {

                    $s_button = "<button class='button-device-delete btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Hapus</button>";
                }
            }
            elseif($a_data[$i]['registerStatus'] == '2')
            {
                $s_button = "<button class='button-device-activation btn btn-flat   btn-success btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Aktifasi Ulang</button>";
            }
            //------------- end button---------------------

                    
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['userID'])."<br>"
                                     ."<p class='hostname'>".$a_data[$i]['realName']."</p>"
                                ."</td>";
            $s_table_container .= "<td  align='left'>"
                                     ."<p class='perangkat'>".strtoupper ($a_data[$i]['deviceModel'])."</p><br>$s_status"
                                ."</td>";
            $s_table_container .= "<td  align='left' >"
                                     .strtoupper ($a_data[$i]['dateRegister'])
                                ."</td>";
            $s_table_container .= "<td  > $s_button</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }


    $CONTENT_MAIN = "
        <!-- BEGIN PAGE CONTENT -->
        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>{$s_table_tile}</div>
                                </div>
                                <div class='card-body'>
                                    <div class='table-responsive'>
                                        {$s_table_container}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
              ";
    $oDevice->closeDB();
    $oGroup->closeDB();

?>