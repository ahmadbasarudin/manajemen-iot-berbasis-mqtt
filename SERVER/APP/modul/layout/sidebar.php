<?php
$real_name = "";
$group_name = "";
$s_menu_container = "";
if(isset($USER[0]['realName']))
{
    $real_name =$USER[0]['realName']; 

    include_once($SYSTEM['DIR_PATH']."/class/class.menu.php");
    $oMenu = new MenuLinks();
    $s_menu_container = $oMenu->buildMenu($USER[0]['userID']);
    $oMenu->closeDB();


    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");

    $oGroup = new Group();
    $a_group_name = $oGroup->getListUserGroup(" WHERE A.userID='{$USER[0]['userID']}' AND A.groupID != 'GRP00000000000000000' ","","");
    if(count($a_group_name)>0)
    {
        $group_name = $a_group_name[0]['name'];
    }
    $oGroup->closeDB();
}



    $LAYOUT_SIDEBAR = "";
    if(isset($USER[0]['avatar']))
    {
        $LAYOUT_SIDEBAR = "
        
            <!-- Sidebar -->
            <div class='sidebar sidebar-style-2' data-background-color='{$SIDEBAR_BACKGROUND}'>           
                <div class='sidebar-wrapper scrollbar scrollbar-inner'>
                    <div class='sidebar-content'>
                        <div class='user'>
                            <div class='avatar-sm float-left mr-2'>
                                <img src='{$USER[0]['avatar']}' alt='...' class='avatar-img rounded-circle'>
                            </div>
                            <div class='info'>
                                <a data-toggle='collapse' href='#collapseExample' aria-expanded='true'>
                                    <span>
                                        {$real_name}
                                        <span class='user-level'>{$group_name}</span>
                                        <span class='caret'></span>
                                    </span>
                                </a>

                                <div class='clearfix'></div>

                                <div class='collapse in' id='collapseExample'>
                                    <ul class='nav'>
                                        <li>
                                            <a href='index.php?page=mode'>
                                                <span class='link-collapse'>Mode Tema</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='index.php?page=user&action=ganti_password'>
                                                <span class='link-collapse'>Ganti Password</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>


                        <!-- menu  -->
                        {$s_menu_container}



                    </div>
                </div>
            </div>
            <!-- End Sidebar -->
        ";
    }
        
?>