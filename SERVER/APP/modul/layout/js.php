<?php

$LAYOUT_JS = "


        <!-- Fonts and icons -->
        <script src='assets/js/plugin/webfont/webfont.min.js'></script>
        <script>
                WebFont.load({
                        google: {'families':['Lato:300,400,700,900']},
                        custom: {'families':['Flaticon', 'Font Awesome 5 Solid', 'Font Awesome 5 Regular', 'Font Awesome 5 Brands', 'simple-line-icons'], urls: ['assets/css/fonts.min.css']},
                        active: function() {
                                sessionStorage.fonts = true;
                        }
                });
        </script>

        <!--   Core JS Files   -->
        <script src='assets/js/core/jquery.3.2.1.min.js'></script>
        <script src='assets/js/core/popper.min.js'></script>
        <script src='assets/js/core/bootstrap.min.js'></script>

        <!-- jQuery UI -->
        <script src='assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'></script>
        <script src='assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>

        <!-- jQuery Scrollbar -->
        <script src='assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js'></script>

        <!-- Sweet Alert -->
        <script src='assets/js/plugin/sweetalert/sweetalert2.min.js'></script>

        <!-- Atlantis JS -->
        <script src='assets/js/apps.min.js'></script>

        {$JS_EXTENDED}

";
?>