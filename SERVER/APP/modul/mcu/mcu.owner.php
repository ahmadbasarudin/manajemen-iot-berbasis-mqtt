<?php
    /*
    |--------------------------------------------------------------------------
    | device iot owner 
    |--------------------------------------------------------------------------
    |Form untuk mengubah owner device iot
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.mcu.php");
    $oMCU = new MCU();
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
    $oUser = new UserInfo();
    if(isset($_REQUEST['device-id']))
    {
        $_REQUEST['device-id'] = $oMCU->antiInjection($_REQUEST['device-id']);
        $a_device = $oMCU->getList("WHERE mcuID = '{$_REQUEST['device-id']}'","","");

        if(count($a_device) > 0 && is_array($a_device))
        {
            //form ganti pelanggan

            $JS_EXTENDED .= "
                            <script src='assets/js/plugin/select2/js/select2.full.min.js'></script>
                            <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                            ";
            $CSS_EXTENDED .= "
                            <link rel='stylesheet' href='assets/js/plugin/select2/css/select2.min.css'>
                            <link rel='stylesheet' href='assets/css/basar_component.css'>
                            ";

            $BUTTON_MAIN  = "
                                <button type='button' id='button-device-owner-apply' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                                    <i class='  fa fa-user'></i> Ganti Pemilik / Pelanggan
                                </button>
                                <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                                    <i class='fa fa-caret-left'></i> kembali
                                </button>
                            ";
            $s_form_input = "";

            $a_data_list_user = $oUser->getUserList(" WHERE TRUE ","","");

            $s_user = "<select class='form-control select2' name='owner'  placeholder='Pembeli / Pemilik' >";
                        $s_user .= "<option value=''>---Belum Terjual---</option>";
                        for($i=0;$i < count($a_data_list_user);$i++)
                        {
                            $selected = "";
                            if($a_data_list_user[$i]['userID'] == $a_device[0]['mcuOwner'])
                            {
                                $selected = " selected = 'selected' ";
                            }
                            $s_user .= "<option value='{$a_data_list_user[$i]['userID']}' $selected>{$a_data_list_user[$i]['realName']}</option>";
                        }
                        $s_user .= "</select>";

            $s_form_input = "
                            <form id='form-owner' action='' method='post'><div >
                                    <!-- /.box-header -->
                                    <div class='box-body'>
                                        <div class='row'>
                                            <div class='col-md-12'>
                                                <div class='input-group mb-3'>
                                                    <div class='input-group'>
                                                        <label>ID Perangkat:</label>
                                                    </div>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                            <i class='fa fas fa-microchip'></i>
                                                        </span>
                                                    </div>

                                                    <input type='text' class='form-control'   disabled value='{$a_device[0]['mcuID']}' >

                                                    <input type='hidden'  name='device-id' value='{$a_device[0]['mcuID']}' >
                                                </div>
                                            </div>
                                            <div class='col-md-12'>
                                                <div class='input-group mb-3'>
                                                    <div class='input-group'>
                                                        <label>Pembeli:</label>
                                                    </div>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' >
                                                            <i class='fa fas fa-user'></i>
                                                        </span>
                                                    </div>
                                                    $s_user
                                                </div>
                                            </div>

                                            
                                            
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </form>";
                                
            $TITLE_MAIN = "Device IoT";
            $TITLE_SUB = "mengganti pembeli / pemilik  perangkat IoT";

            $BUTTON_ACTION = "
                                        <button id='button-back' class='  btn btn-warning  btn-round'>
                                            <span class='btn-label'>
                                                <i class='fa fas fa-caret-left'></i>
                                            </span>
                                            Kembali
                                        </button>
                                        <button id='button-device-owner-apply' class='btn btn-success  btn-round'>
                                            <span class='btn-label'>
                                                <i class='fa fas fa-user'></i>
                                            </span>
                                            Ganti Pemilik / Pembeli 
                                        </button>
            ";

                    
            $CONTENT_MAIN = "

            <!-- BEGIN PAGE CONTENT -->


                <div class='main-panel'>
                    <div class='content'>
                        <div class='page-inner'>
                            <div class='row'>

                                <div class='col-md-12'>
                                    <div class='card'>
                                        <div class='card-header'>
                                            <div class='card-title'>Form Pergantian Pemilik / Pembeli Perangkat IoT</div>
                                        </div>
                                        <div class='card-body'>
                                            <div >
                                                {$s_form_input}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            <!-- END PAGE CONTENT -->
                      ";
        }
    }
    $oMCU->closeDB();
    $oUser->closeDB();