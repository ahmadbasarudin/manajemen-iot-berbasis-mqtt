<?php
    /*
    |--------------------------------------------------------------------------
    | device iot Create 
    |--------------------------------------------------------------------------
    |Form untuk entry device iot
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.mcu.php");
    $oMCU = new MCU();
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
    $oUser = new UserInfo();

    $JS_EXTENDED .= "
                    <script src='assets/js/plugin/select2/js/select2.full.min.js'></script>
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/js/plugin/select2/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/basar_component.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-create-apply' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus'></i> Tambah
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";


    $a_data_list_user           = $oUser->getUserList(" WHERE TRUE ","","");
    $a_data_list_mcu_category   = $oMCU->getCatList(" WHERE TRUE ","","");

    $s_user = "<select class='form-control select2' name='owner'  placeholder='Pembeli' >";
                $s_user .= "<option value=''>---Belum Terjual---</option>";
                for($i=0;$i < count($a_data_list_user);$i++)
                {
                    $s_user .= "<option value='{$a_data_list_user[$i]['userID']}'>{$a_data_list_user[$i]['realName']}</option>";
                }
                $s_user .= "</select>";

    $s_mcu_cat = "<select class='form-control select2' name='mcu-category'  placeholder='Kategori MCU' >";
                $s_mcu_cat .= "<option value=''>---Pilih Kategori MCU---</option>";
                for($i=0;$i < count($a_data_list_mcu_category);$i++)
                {
                    $s_mcu_cat .= "<option value='{$a_data_list_mcu_category[$i]['mcuCatID']}'>{$a_data_list_mcu_category[$i]['mcuName']}</option>";
                }
                $s_mcu_cat .= "</select>";

    $s_form_input = "
                    <form id='form-create' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='input-group mb-3'>
                                            <div class='input-group'>
                                                <label>ID Perangkat:</label>
                                            </div>
                                            <div class='input-group-prepend'>
                                                <span class='input-group-text' >
                                                    <i class='fa fas fa-microchip'></i>
                                                </span>
                                            </div>
                                                <input type='text' class='form-control' name='device_id' placeholder='mac address wifi'>
                                        </div>
                                    </div>
                                    <div class='col-md-12'>
                                        <div class='input-group mb-3'>
                                            <div class='input-group'>
                                                <label>Kategori MCU:</label>
                                            </div>
                                            <div class='input-group-prepend'>
                                                <span class='input-group-text' >
                                                    <i class='fa fas fa-user'></i>
                                                </span>
                                            </div>
                                            $s_mcu_cat
                                        </div>
                                    </div>

                                    <div class='col-md-12'>
                                        <div class='input-group mb-3'>
                                            <div class='input-group'>
                                                <label>Pembeli:</label>
                                            </div>
                                            <div class='input-group-prepend'>
                                                <span class='input-group-text' >
                                                    <i class='fa fas fa-user'></i>
                                                </span>
                                            </div>
                                            $s_user
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
    $TITLE_MAIN = "Device IoT";
    $TITLE_SUB = "menambah perangkat IoT";

    $BUTTON_ACTION = "
                                <button id='button-back' class='  btn btn-warning  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-caret-left'></i>
                                    </span>
                                    Kembali
                                </button>
                                <button id='button-device-create-apply' class='btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-plus'></i>
                                    </span>
                                    Tambah 
                                </button>
    ";

            
    $CONTENT_MAIN = "

    <!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card bg-warning text-white'>
                                <div class='card-body'>
                                    <div >
                                        <h4> Petunjuk Pengisian</h4>
                                        <b>ID Perangkat</b> merupakan mac address yang diutamakan diambil dari mac address wifi. Apabila tidak tersedia maka dapat diambil dari UUID OS MCU<br />
                                        <b>Pembeli</b> diisi dengan nama pembeli. kalau alat belum terjual maka bisa dikosongi<br />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Form Perangkat IoT</div>
                                </div>
                                <div class='card-body'>
                                    <div >
                                        {$s_form_input}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
    $oMCU->closeDB();
    $oUser->closeDB();

?>