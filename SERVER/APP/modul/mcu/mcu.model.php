<?php
/*
|--------------------------------------------------------------------------
| Device Iot Model
|--------------------------------------------------------------------------
|
|Device Iot login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
    $PAGE_MODEL = true; // khusus untuk halaman model
    
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.mcu.php");
    $oMCU = new MCU();
	//jika variable error sudah terisi maka kosongkanlah
	$response['status'] = "error";
	$response['desc'] = "";

	$s_error  = "";
    $a_errors = array();
    if(isset($_REQUEST['action']))
    {
        
        if($_REQUEST['action'] == 'delete')
        {
            if($_REQUEST['mcuID'] == "")
            {
                $a_errors[] = "device iot tidak ada";
            }
            if (!$a_errors) 
            {
                //update di datauser
                if($oMCU->delete($_REQUEST['mcuID']))
                {
                    $response['status'] = 'success';
                    $response['desc'] = "device berhasil dihapus";

                }
                else
                {

                    $response['status'] = 'error';
                    $response['desc'] = "device gagal dihapus";
                }
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                    $s_error .= "$error<br />";
                }
                $response['status'] = 'error';
                $response['desc'] = $s_error;
            }
        }

        elseif($_REQUEST['action'] == 'create')
        {
            if(!isset($_REQUEST['device_id']))
            {
                $a_errors[] = "kode perangkat tidak ada";
            }
            else
            {
                if($_REQUEST['device_id'] == "")
                {
                    $a_errors[] = "kode perangkat harus diisi";
                }
            }

            if(!isset($_REQUEST['mcu-category']))
            {
                $a_errors[] = "kategori MCU tidak ada";
            }
            else
            {
                if($_REQUEST['mcu-category'] == "")
                {
                    $a_errors[] = "kategori MCU harus diisi";
                }
            }
            if(!isset($_REQUEST['owner']))
            {
                $_REQUEST['owner'] = "";
            }
            if (!$a_errors) 
            {
                $_REQUEST['owner'] = $oMCU->antiInjection($_REQUEST['owner']);
                $_REQUEST['device_id'] = $oMCU->antiInjection($_REQUEST['device_id']);
                $_REQUEST['mcu-category'] = $oMCU->antiInjection($_REQUEST['mcu-category']);
                //jika kode pernah dimasukan sebelumnya
                if($oMCU->getCount(" WHERE mcuID = '{$_REQUEST['device_id']}'") > 0)
                {

                    $response['status'] = 'error';
                    $response['desc'] = "kode perangkat sudah pernah dimasukan sebelumnya";
                }
                else
                {
                    $a_device['mcuID'] = $_REQUEST['device_id'];
                    $a_device['mcuOwner'] = $_REQUEST['owner'];
                    $a_device['mcuUserCreate'] = $USER[0]['userID'];
                    $a_device['mcuDateCreate'] = date('Y-m-d');
                    $a_device['mcuRegistrationID'] = md5($a_device['mcuID'] . date('Ymdhis'));
                    $a_device['mcuCatID'] = $_REQUEST['mcu-category'];
                    $a_device['mcuStatus'] = 1;
                    
                    if($oMCU->create($a_device))
                    {

                        $response['status'] = 'success';
                        $response['desc'] = "data perangkat berhasil dimasukan";
                        
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['desc'] = "sistem tidak dapat memasukan data perangkat";

                    }
                }
            }
        }
        elseif($_REQUEST['action'] == 'owner')
        {
            if(!isset($_REQUEST['device-id']))
            {
                $a_errors[] = "kode perangkat tidak ada";
            }
            else
            {
                if($_REQUEST['device-id'] == "")
                {
                    $a_errors[] = "kode perangkat harus diisi";
                }
            }
            if(!isset($_REQUEST['owner']))
            {
                $_REQUEST['owner'] = "";
            }
            if (!$a_errors) 
            {
                $_REQUEST['owner'] = $oMCU->antiInjection($_REQUEST['owner']);
                $_REQUEST['device-id'] = $oMCU->antiInjection($_REQUEST['device-id']);
                //jika kode pernah dimasukan sebelumnya
                if($oMCU->getCount(" WHERE mcuID = '{$_REQUEST['device-id']}'") <= 0)
                {

                    $response['status'] = 'error';
                    $response['desc'] = "kode perangkat tidak ditemukan";
                }
                else
                {
                    $a_device['mcuID'] = $_REQUEST['device-id'];
                    $a_device['mcuOwner'] = $_REQUEST['owner'];
                    
                    if($oMCU->owner($a_device))
                    {

                        $response['status'] = 'success';
                        $response['desc'] = "data pemilik / pembeli berhasil dimasukan";
                        
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['desc'] = "sistem mengganti pemilik / pembeli";

                    }
                }
            }
        }

        if ($a_errors) 
        {
            $s_error =  '';
            foreach ($a_errors as $error) 
            {
                $s_error .= "$error<br />";
            }
            $response['status'] = 'error';
            $response['desc'] = $s_error;
        }
    }
	echo json_encode($response);
	$oMCU->closeDB();

?>