$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-COMPUTER
    **************************************************/

    if($('.button-create').length)
    {
        jQuery('.button-create').click(function() {
            document.location='index.php?page=mcu&action=create'
        });
    };
    if($('#table-device').length)
    {
        
        $('#table-device').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 0, 2 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[10, 25, -1], [10, 25, "All"]],
             order: [[ 2, "desc" ]]
        });

        $('#table-device tbody').on( 'click', '.button-owner', function () 
        {
            device_id = $(this).attr('record-id'); 
            document.location='index.php?page=mcu&action=owner&device-id='+device_id;
        });

        $('#table-device tbody').on( 'click', '.button-delete', function () 
        {
            device_id = $(this).attr('record-id'); 
            Swal.fire({
                    title: 'Konfirmasi',
                    html: "Apakah  yakin  device iot <b>"+device_id +"</b> dihapus?",
                    type: 'warning',
                    onOpen: () => {
                        var zippi = new Audio('assets/sound/warning.mp3')
                        zippi.play();
                    },
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, dihapus saja',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) 
                    {

                        $.ajax({
                            type: 'POST',
                            url: "index.php?page=mcu&type=model&action=delete",
                            dataType:"json",
                            data: 'mcuID='+device_id,
                            cache:false,
                            //jika complete maka
                            complete:
                                function(data,status)
                                {
                                 return false;
                                },
                            success:
                                function(msg,status)
                                {
                                    //alert(msg);
                                    $('#notification-container').hide();
                                    if(msg.status == 'success')
                                    {
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Success...',
                                            text: msg.desc,
                                            timer: 1200,
                                            onOpen: () => {
                                                var zippi = new Audio('assets/sound/success.mp3')
                                                zippi.play();
                                            },
                                            onClose: () => {
                                                document.location='index.php?page=mcu&action=list'
                                            }
                                        })


                                    }
                                    else
                                    {
                                        $('#notification-container').removeClass('alert-success');
                                        $('#notification-container').addClass('alert-danger'); 
                                        $('#notification-content').html(msg.desc);
                                        $('#notification-container').show(100); 
                                    }
                                    $('html, body').animate({
                                    scrollTop: $('#container-alert').offset().top
                                    }, 500);

                                },
                            //untuk sementara tidak digunakan
                            error:
                                function(msg,textStatus, errorThrown)
                                {
                                    $('#notification-container').hide();
                                    $('#notification-container').removeClass('alert-success');
                                    $('#notification-container').addClass('alert-danger'); 
                                    $('#notification-content').html('Terjadi kegagalan proses transaksi');
                                    $('#notification-container').show(); 
                                    $('html, body').animate({
                                    scrollTop: $('#container-alert').offset().top
                                    }, 500);
                                }
                        });//end of ajax
                    }
            });

        });
    }
    /*   END TABLE-COMPUTER
    **************************************************/


    if($('#button-device-owner-apply').length)
    {
        jQuery('#button-back').click(function() {
            document.location='index.php?page=mcu&action=list'
        });

        $('#button-device-owner-apply').click(function() 
        {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=mcu&type=model&action=owner",
                    dataType:"json",
                    data: $("#form-owner").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.status == 'success')
                            {


                                Swal.fire({
                                    type: 'success',
                                    title: '',
                                    text: msg.desc,
                                    timer: 1200,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/success.mp3')
                                        zippi.play();
                                    },
                                    onClose: () => {
                                        document.location='index.php?page=mcu&action=list';
                                    }
                                });

                            }
                            else
                            {
                                
                                Swal.fire({
                                    type: 'error',
                                    title: '',
                                    html: msg.desc,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/error.mp3')
                                        zippi.play();
                                    }
                                });
                            }

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            
                            Swal.fire({
                                type: 'error',
                                title: '',
                                text: 'Terjadi error saat proses kirim data',
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
                });//end of ajax

        });
    }
    if($('#button-device-create-apply').length)
    {
        jQuery('#button-back').click(function() {
            document.location='index.php?page=mcu&action=list'
        });
        $('#button-device-create-apply').click(function() 
        {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=mcu&type=model&action=create",
                    dataType:"json",
                    data: $("#form-create").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.status == 'success')
                            {


                                Swal.fire({
                                    type: 'success',
                                    title: '',
                                    text: msg.desc,
                                    timer: 1200,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/success.mp3')
                                        zippi.play();
                                    },
                                    onClose: () => {
                                        document.location='index.php?page=mcu&action=list';
                                    }
                                });

                            }
                            else
                            {
                                
                                Swal.fire({
                                    type: 'error',
                                    title: '',
                                    html: msg.desc,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/error.mp3')
                                        zippi.play();
                                    }
                                });
                            }

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            
                            Swal.fire({
                                type: 'error',
                                title: '',
                                text: 'Terjadi error saat proses kirim data',
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
                });//end of ajax

        });
    }
    
    if($('.select2').length)
    {
        $('.select2').select2();
    }

});