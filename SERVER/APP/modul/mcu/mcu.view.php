<?php
    /*
    |--------------------------------------------------------------------------
    | device view
    |--------------------------------------------------------------------------
    |view  modul device register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    //$PAGE_ID = "UDR102";
    //require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    
    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
        } 
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.create.php");
        }
        elseif($_REQUEST['action'] == "owner")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.owner.php");
        }

    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
    }

?>