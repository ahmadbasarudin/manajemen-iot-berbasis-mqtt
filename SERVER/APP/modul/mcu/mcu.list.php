<?php
    /*
    |--------------------------------------------------------------------------
    | device list
    |--------------------------------------------------------------------------
    |list  modul device register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    //$PAGE_ID = "UDR103";
    //require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.mcu.php");
    $oMCU = new MCU();
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    $oGroup = new Group();


    /**
    *   configuration variable
    **/
    $TITLE_MAIN = "Manajemen MCU";
    $TITLE_SUB = "";
    $button['create']['text'] = "Tambah MCU";


    $table['title']   = "MCU Device";
    $table['header']['title'][]   = "MAC ADDRESS";
    $table['header']['title'][]   = "PEMBELI";
    $table['header']['title'][]   = "";
    $table['header']['class'][]   = " ";
    $table['header']['class'][]   = " style='width:200px;' ";
    $table['header']['class'][]   = " style='width:10px;' ";

    $a_title[] = "MAC ADDRESS";
    $a_title[] = "PEMBELI";
    $a_title[] = "";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:10px;' ";

    $s_table_container = "";
    $s_condition = " WHERE mcuRegistrationStatus =1 ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `mcuDateCreate` DESC ";


    $JS_EXTENDED .= "
                    <script src='assets/js/plugin/datatables/datatables.min.js'></script>
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";


    $a_data = $oMCU->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data) && is_array($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-device' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($table['header']['title']);$i++)
                                   {
                                        $s_table_container .="<td {$table['header']['class'][$i]} >" .$table['header']['title'][$i]."</td>";
                                   }
        $s_table_container .="</tr>
                        </thead>";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {
            $s_status = "";
            $s_button = "";
            $s_bg_column = "";


            $s_button .= "<a class='button-owner dropdown-item' href='#' record-id='{$a_data[$i]['mcuID']}' >Ganti Pembeli</a>
                                    ";
            $s_button .= "<a class='button-delete dropdown-item' href='#' record-id='{$a_data[$i]['mcuID']}' >Hapus</a>
                                    ";

            $BUTTON_RECORD = "
                                <div class='btn-group'>
                                    <button type='button' class='btn btn-info btn-flat' data-toggle='dropdown' >Aksi</button>
                                    <button type='button' class='btn btn-info btn-flat dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                                        <span class='sr-only'>Toggle Dropdown</span>
                                    </button>
                                    <div class='dropdown-menu' role='menu' x-placement='bottom-start' style='position: absolute; transform: translate3d(87px, 43px, 0px); top: 0px; left: 0px; will-change: transform;'>
                                        {$s_button}
                                    </div>
                                </div>
            ";
            //$s_button = "<button class='button-delete btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['diID']}'  style='margin-left: 2px;'>Hapus</button>";


                    
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['mcuID'])."<br>"
                                ."</td>";
            $s_table_container .= "<td  align='left' >"
                                     .strtoupper ($a_data[$i]['realName'])
                                ."</td>";
            $s_table_container .= "<td  > $BUTTON_RECORD</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }




    $BUTTON_ACTION = "
                                <button class='button-create  btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-plus'></i>
                                    </span>
                                    {$button['create']['text']}
                                </button>
    ";
    $CONTENT_MAIN = "
        <!-- BEGIN PAGE CONTENT -->
        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-body'>
                                    <div class='table-responsive'>
                                        {$s_table_container}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
              ";
    $oMCU->closeDB();
    $oGroup->closeDB();

?>