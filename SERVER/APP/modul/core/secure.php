<?php


    /*
    |--------------------------------------------------------------------------
    | Secure Enhanced
    |--------------------------------------------------------------------------
    |Digunakan setiap kali transaksi sesudah login
    |Hanya berkaitan dengan keamanan akses file diluar alamat karena keamanan alamat sudah ditangani firewall
    |
    |
    |diperbolehkan siapa saja yang mengakses dengan membandingkan page_id dicocokan dengan database
    */

    require_once($SYSTEM['DIR_PATH']."/class/function.array.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUserInfo = new UserInfo();/**
    * jika tidak di login masukan ke form login.
    * jika sudah maka user akan dapat melihat halaman selanjutnya
    */
    //periksa apakah user sudah login
    if($oUserInfo->isLoggedIn())
    {
        //dapatkan user  data yang sedang Login
        $oUserInfo->setUsernameActive($_SESSION['username']);
        $try_login = $oUserInfo->getUsernameInfo();

        if(isset($try_login[0]))
        {
            //jika user telah login di browser / komputer lain
            // bandingkan userID yang di system dengan yang dicatat dari database
            if(session_id()  != $try_login[0]['lastSession'])
            {
                //hancurkan session
                $oUserInfo->logout();
                @header("Location:login/12");
            }
            else
            {
                $PRIVILEGES=  $oUserInfo->getUserPrivileges($try_login[0]['userID']);
                $USER = $oUserInfo->getUserDetail($try_login[0]['userID']);         
                if(isset($USER[0]['userID']))
                {

                    if($USER[0]['avatar'] == "")
                    {
                        $USER[0]['avatar'] = $NO_PHOTO;
                    }
                    $a_nama_panggilan = explode(" ", $USER[0]['realName']);
                    if(count($a_nama_panggilan) > 0)
                    {
                        $USER[0]['panggilan'] = $a_nama_panggilan[0]; 
                    }
                    else
                    {
                        $USER[0]['panggilan'] = $USER[0]['realName'];
                    }
                    $USER[0]['accessLevel'] =  $oUserInfo->getUserPrivileges($USER[0]['userID']);
                    if($USER[0]['accessLevel'])
                    {
                        $try_login[0]['previleges'] = $PRIVILEGES;
                        if(deepInArray($PAGE_ID,$PRIVILEGES))
                        {
                            // halaman telah lolos validasi
                        }
                        else
                        {
                            //tolak dan dibawa ke index.php karena tidak mempunyai hak akses untuk halaman ini
                            if($PAGE_MODEL)
                            {
                                $PAGE_MODEL = false;
                                header("Location:index.php?page=deniedmodel&error_code={$PAGE_ID}");
                            }
                            else
                            {

                                header("Location:index.php?page=denied&error_code={$PAGE_ID}");
                            }
                        }
                    }
                    else
                    {
                        //tolak dan dibawa ke index.php karena tidak mempunyai hak akses apapun
                        header("Location:index.php?page=denied");
                    }
                }
                else
                {
                    //user tidak ditemukan
                    header("Location:login/15");
                }
            }
        }
    }
    //jika user belum login
    else
    {
        header("Location:login");
    }
    $oUserInfo->closeDB();
?>