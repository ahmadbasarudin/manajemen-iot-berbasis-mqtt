
<?php
/**
 *
 * @version     1.0
 * @author      basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/

    $PAGE_ID = "IND100";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    $LAYOUT_CSS_CONTENT=    "
                            ";
    $LAYOUT_JS_CONTENT =   "
                           ";

?>
<?php
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                        <section class='page-head'>
                            <div class='row'>
                                <h4>WELCOME</h4>
                                <hr>
                            </div>
                        </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
?>