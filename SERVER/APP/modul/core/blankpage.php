
<?php
/**
 *
 * @version     1.0
 * @author      basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/

    $PAGE_ID = "IND100";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    $LAYOUT_CSS_CONTENT=    "
        <link rel='stylesheet' href='assets/css/welcome.css'>
                            ";
    $LAYOUT_JS_CONTENT =   "    
        <script src='assets/js/welcome.js'></script>
                           ";

?>
<?php



    $TITLE_MAIN = "Blank Page";
    $TITLE_SUB = "basic page";

    $BUTTON_ACTION = "
                                <button class='btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-check'></i>
                                    </span>
                                    Blank Page
                                </button>
    ";
    $CONTENT_MAIN = "
                

<!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Card Title</div>
                                    <div class='card-category'>Card Category</div>
                                </div>
                                <div class='card-body'>
                                    Card Body
                                </div>
                                <div class='card-footer'>
                                    <hr>
                                    Card Footer
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
    include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
?>