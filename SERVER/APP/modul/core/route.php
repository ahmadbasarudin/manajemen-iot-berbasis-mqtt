<?php
    /*
    |--------------------------------------------------------------------------
    | Route
    |--------------------------------------------------------------------------
    |Routing dari parameter URI
    |Khusus untuk modul login menyertakan modul firewall.php,selain modul login menggunakan modul secure.php
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string

    |masih belum memanfaatkan htaccess
    |variable standart :
    |   page -> digunakan untuk mengakses halaman
    |   type -> 0 = view, 1 = model ; 0 default
    |   action -> list,detail,edit,delete
    |   record_id -> string untuk menujukan id dari primary key table
    */



    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUserInfo = new UserInfo();

    //jika sudah login
    if($oUserInfo->isLoggedIn())
    {
        if(isset($_REQUEST['page']))
        {
            //include("secure.php");
            switch ($_REQUEST['page']) 
            {

                // About page
                case '':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/welcome.php");
                    break;
                case 'welcome':
                    require_once($SYSTEM['DIR_MODUL']."/welcome/welcome.php");
                    break;
                case 'logout':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/logout.php");
                    break;
                case 'denied':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/denied.php");
                    break;
                case 'deniedmodel':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/deniedmodel.php");
                    break;

                case 'user':
                    require_once($SYSTEM['DIR_MODUL']."/user/user.php");
                    break;
                case 'device':
                    require_once($SYSTEM['DIR_MODUL']."/device_register/device_register.php");
                    break;
                case 'mcu':
                    require_once($SYSTEM['DIR_MODUL']."/mcu/mcu.php");
                    break;
                case 'pelangganpascabayar':
                    require_once($SYSTEM['DIR_MODUL']."/pelanggan_pascabayar/pelanggan_pascabayar.php");
                    break;
                case 'dashboard':
                    require_once($SYSTEM['DIR_MODUL']."/dashboard/dashboard.php");
                    break;
                case 'notification':
                    require_once($SYSTEM['DIR_MODUL']."/notification/notification.php");
                    break;
                case 'laporan':
                    require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.php");
                    break;
                case 'wilayah':
                    require_once($SYSTEM['DIR_MODUL']."/wilayah/wilayah.php");
                    break;
                case 'mode':
                    require_once($SYSTEM['DIR_MODUL']."/mode/mode.php");
                    break;
                // Everything else
                default:
                    //header('HTTP/1.0 404 Not Found');
                    require_once($SYSTEM['DIR_MODUL']."/login/login.php");
                    break;
            }//end switch   
        }
        else
        {
            require_once($SYSTEM['DIR_MODUL_CORE']."/welcome.php");
        }
        
    }
    //jika belum login
    else
    {
        //default adalah login
        include_once($SYSTEM['DIR_MODUL_CORE']."/firewall.php");
        require_once($SYSTEM['DIR_MODUL']."/login/login.php");
    }
    //$oUserInfo->closeDB();

?>