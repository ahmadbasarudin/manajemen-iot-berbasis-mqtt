<?php
/*
|--------------------------------------------------------------------------
| Array
|--------------------------------------------------------------------------
|
|menyeleksi variable array dengan fungsi deepInArray
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
    //digunakan untuk mencari data didalam array
    //jika data tersebut ada maka akan mengembelikan true jika tidak false
    //parameter dimasukan adalah 2
    //value tidak boleh boolean
    function deepInArray($_uk_value, $_a_array) 
    {
        foreach($_a_array as $aItem) 
        {
            if(!is_array($aItem)) 
            {
                if(is_bool($aItem))
                {
                    return false;
                }
                elseif(!isset($aItem))
                {
                    return false;
                }
                else
                {
                    if ($aItem == $_uk_value) return true;
                    else continue;
                }
            }
            else
            {
                if(in_array($_uk_value, $aItem,true)) return true;
                else if(deepInArray($_uk_value, $aItem)) return true;
            }
        }
        return false;
    }

    function arrayToSerial($_delimiter,$_array)
    {
        $sString = "";
        for($i=0;$i<count($_array);$i++)
        {
            if($i==0)
            {
                $sString .= $_array[$i];
            }
            else
            {
                $sString .= $_delimiter.$_array[$i];
            }
        }
        return $sString;
    }

    function serialToArray($_delimiter,$_string)
    {
        return explode($_delimiter, $_string);
    }
?>