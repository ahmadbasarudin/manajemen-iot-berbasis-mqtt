<?php

/*
|--------------------------------------------------------------------------
| Firewall
|--------------------------------------------------------------------------
|
|diperbolehkan siapa saja yang mengkases dengan membandingkan ip address / imei sewaktu login
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
|    
*/


include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Firewall extends  masterDB
{
     /**
     * Constructor
     */
     function __construct()
     {
          parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
     }

     /**
     * periksa list firewall yang ada didatabase
     *
     * @return array list firewall
     */
     function getList($_condition,$_order,$_limit)
     {
          $sql =    "    SELECT *
                         FROM `firewall` 

                         {$_condition}  {$_order} {$_limit} 

                          " ;
          return $this->getResult($sql);
     }
     /**
     * periksa jumlah baris pada firewall
     *
     * @return integer total
     */
     function getCount($_condition)
     {
          $sql =    "    SELECT count(*) as total 
                         FROM `firewall` 
                         {$_condition} ";
          $aHasil =  $this->getResult($sql);
          return $aHasil[0]['total'];
     }

     /**
     * delete
     *
     * @return bool true/false
     */
     function delete($_user_id,$_client_address)
     {
          $condition   = " WHERE  `userID` =  '$_user_id';  ";
          //jika  userID  hanya mempunyai alamat client adderss satu atau kurang maka tidak dapat dihapus
          if($this->getCount($condition) > 1 )
          {
               $a_query[] = " DELETE FROM 'firewall' " . $condition;
               return $this->queryTransaction($a_query);
          }
          else
          {
               return false;
          }
     }
}
?>