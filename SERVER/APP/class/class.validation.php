<?php

/*
|--------------------------------------------------------------------------
| Validation
|--------------------------------------------------------------------------
|
|class memvalidasi variaBEL
|   INPUT
|		array $_request, array $_compare
|	OUTPUT
|		array error	
|    
*/

class Validation
{
	var  $debug = 0; //array
	function check($_request,$_compare)
	{

    		$a_errors = array();
		if(isset($_request))
		{

			for($i=0;$i<count($_compare);$i++)
			{
				if(!isset($_request[$_compare[$i]['index-name']]))
				{
					$a_errors[] =   $_compare[$i]['not-isset'];
				}
				else
				{
					if($_request[$_compare[$i]['index-name']] == "")
					{
						$a_errors[] =   $_compare[$i]['is-blank'];
					}
				}
			}
			
		}
		return $a_errors;
	}



}
