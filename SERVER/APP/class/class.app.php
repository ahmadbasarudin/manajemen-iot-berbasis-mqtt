<?php
/*
|--------------------------------------------------------------------------
| Application
|--------------------------------------------------------------------------
|
|class untuk menghandle application control
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Application extends  masterDB
{
    function __construct()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

    /**
    * periksa list ticket yang ada didatabase
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT *
                        FROM `appVersion`
                        {$_condition}  {$_order} {$_limit} 

                    " ;
        return $this->getResult($sql);
    }

    function getCount($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `appVersion` 
                    {$_condition} ";
        $a_hasil =  $this->getResult($sql);
        return $a_hasil[0]['total'];

    }

    function getVersion($_condition)
    {
        $sql =  "   SELECT lastVersion  
                    FROM `appVersion` 
                    {$_condition} ";
        $a_hasil =  $this->getResult($sql);
        return $a_hasil[0]['lastVersion'];

    }
}
?>