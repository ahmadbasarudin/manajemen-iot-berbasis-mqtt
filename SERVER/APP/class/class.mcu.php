<?php
/*
|--------------------------------------------------------------------------
| Device
|--------------------------------------------------------------------------
|
|menghandle device android yang digunakan login aplikasi
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class MCU extends  masterDB
{
    /**
    * Constructor
    */
    function __construct()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

     /**
    * periksa list mcu yang ada didatabase
    *
    * @return array list mcu
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =    "     
                    SELECT M.*,U.realName
                    FROM `mcu`  M
                    LEFT JOIN `user` U ON U.userID = M.mcuOwner
                    {$_condition}  {$_order} {$_limit} 

        " ;
        //print_r($sql); exit();
        return $this->getResult($sql);
    }

     /**
    * periksa list mcu category yang ada didatabase
    *
    * @return array list mcu category
    */
    function getCatList($_condition,$_order,$_limit)
    {
        $sql =    "     
                    SELECT mcuCatID,mcuName
                    FROM `mcuCategory` 
                    {$_condition}  {$_order} {$_limit} 

        " ;
        //print_r($sql); exit();
        return $this->getResult($sql);
    }
    /**
    * periksa jumlah baris pada device
    *
    * @return integer total
    */
    function getCount($_condition)
    {
        $sql =    "     SELECT count(*) as total 
                        FROM `mcu` 
                        {$_condition} ";
        $a_hasil =  $this->getResult($sql);
        return $a_hasil[0]['total'];
    }

    function delete($_device_id)
    {

        $a_query[] = "  UPDATE  `mcu` 
                        SET     `mcuRegistrationStatus` = '2' 
                        WHERE   `mcu`.`mcuID` = '{$_device_id}';";
                        
        return $this->queryTransaction($a_query);
    }

    function customer($a_device)
    {
        $a_query[] = "  UPDATE  `mcu` 
                        SET     `mcuCustomerID` = '{$a_device['customer']}' 
                        WHERE   `mcu`.`mcuID` = '{$a_device['mcuID']}';";
        return $this->queryTransaction($a_query);
    }
    function owner($a_device)
    {
        $a_query[] = "  UPDATE  `mcu` 
                        SET     `mcuOwner` = '{$a_device['mcuOwner']}' 
                        WHERE   `mcu`.`mcuID` = '{$a_device['mcuID']}';";
        return $this->queryTransaction($a_query);
    }

    function create($a_device)
    {
        if($a_device['mcuOwner'] == "")
        {
            $a_device['mcuOwner'] = " null "; 
        }
        else
        {
            $a_device['mcuOwner'] = "'".$a_device['mcuOwner']."'";
        }

        $a_query[] = "  INSERT INTO `mcu` 
                        (`mcuID`,`mcuCatID`, `mcuOwner`,`mcuRegistrationID`, `mcuUserCreate`, `mcuDateCreate`, `mcuRegistrationStatus`) 
                        VALUES (
                            '{$a_device['mcuID']}', 
                            '{$a_device['mcuCatID']}', 
                            {$a_device['mcuOwner']},
                            '{$a_device['mcuRegistrationID']}',
                            '{$a_device['mcuUserCreate']}',
                            '{$a_device['mcuDateCreate']}',
                            '{$a_device['mcuStatus']}'
                        ) ";
        return $this->queryTransaction($a_query); 
    }

    //berupa array
    function historyAdd($_device_history)
    {

        $a_query[] = "  INSERT INTO `mcuHistory` 
                        (`mcuID`, `dihDate`,`dihDesc`, `dihMessage`) 
                        VALUES (
                            '{$_device_history['mcuID']}', 
                            NOW(),
                            '{$_device_history['dihDesc']}',
                            '{$_device_history['dihMessage']}'
                        ) ";
        return $this->queryTransaction($a_query); 
    }
}

?>