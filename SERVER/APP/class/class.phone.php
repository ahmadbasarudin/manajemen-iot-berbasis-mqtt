<?php
/*
|--------------------------------------------------------------------------
| Device
|--------------------------------------------------------------------------
|
|menghandle device android yang digunakan login aplikasi
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/


include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Phone extends  masterDB
{
    /**
    * Constructor
    */
    function __construct()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }


     /**
    * periksa list device yang ada didatabase
    *
    * @return array list device
    */
    function getList($_condition,$_order,$_limit)
    {
        /*
            untuk enable/disable 
                active              -> user
                registrationStatus  -> phone
        */
        $sql 	=	"     
	                    SELECT U.mail,U.realName,U.active,PR.* 
	                    FROM `phoneRegister` PR 
	                    LEFT JOIN user U on U.userID = PR.userID   
	                    {$_condition}  {$_order} {$_limit} 
        			";
        //print_r($sql); exit();
        return $this->getResult($sql);
    } 

    /**
    * periksa jumlah baris pada phone register
    *
    * @return integer total
    */
    function getCount($_condition)
    {
        $sql =    "     SELECT count(*) as total 
                        FROM `phoneRegister` 
                        {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }



    /**
    * memasukan record phone
    *
    * @return bool
    */
    function create($_a_data)
    {


        //db field = index post   
        $request    =   [
                            "userID"            =>  $_a_data['user-id'],
                            "appID"             =>  $_a_data['app-name'],
                            "appVersion"        =>  $_a_data['app-version'],
                            "deviceModel"       =>  $_a_data['device-model'],
                            "deviceSDK"         =>  $_a_data['device-sdk'],
                            "deviceMerk"        =>  $_a_data['device-merk'],
                            "deviceVersion"    =>  $_a_data['device-version'],
                            "deviceID"         =>  $_a_data['unique-identifier'],
                            "screenSize"        =>  $_a_data['screen-size'],
                            "language"          =>  $_a_data['language'],
                            "country"           =>  $_a_data['country'],
                            "registrationDate"  =>  $_a_data['registration-date'],
                            "registrationToken" =>  $_a_data['registration-token']
                        ]; 

        $sql[] = $this->sqlInsert("phoneRegister",$request);
        //echo $sql;
        return $this->queryTransaction($sql);




    }



    
}







?>