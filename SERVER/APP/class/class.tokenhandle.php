<?php
/*
|--------------------------------------------------------------------------
| TokenHandle
|--------------------------------------------------------------------------
|
|untuk membuat token agar akses user non login bisa kirim data
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class TokenHandle extends  masterDB
{

    /**
    * Constructor
    */
    function __construct()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

    function create($s_user_id,$s_expired)
    {
        $token = date("Ymd")."_".md5(date("His"));
    	$a_query[] = "	INSERT INTO `tokenHandle` 
							(`tokenID`, `createUser`, `createDate`, `expiredDate`, `status`) 
						VALUES ('{$token}', '{$s_user_id}', now(),'$s_expired', '0');";
        //echo $a_query[0];
        return $this->queryTransaction($a_query);
    }
    function lastToken()
    {

        // periksa username dan password apakah berada disini
        $sql = "	SELECT tokenID 
        			FROM `tokenHandle` 
        			ORDER BY `tokenHandle`.`createDate` 
        			DESC limit 1 ";
        $a_hasil =  $this->getResult($sql);
        return $a_hasil[0]['tokenID'];
    }

    function isExist($_token_id)
    {

        // periksa username dan password apakah berada disini
        $sql = "
                    SELECT count(*) as total 
                    FROM `tokenHandle` 
                    WHERE tokenID=  '{$_token_id}' AND  `status` =  '0';
                       ";
        $a_hasil =  $this->getResult($sql);
        if($a_hasil[0]['total'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function isExpired($_token_id)
    {
        if($this->isExist($_token_id))
        {
             // periksa username dan password apakah berada disini
            $sql = "
                        SELECT  expiredDate
                        FROM `tokenHandle` 
                        WHERE tokenID=  '{$_token_id}';
                           ";
            $a_hasil =  $this->getResult($sql);
            if(strtotime($a_hasil[0]['expiredDate']) < strtotime(date("Y-m-d H:i:s")))
            {

                return true;
            } 
            else
            {
                return false;
            }  
        }
        else
        {
            return false;
        }
    }
    function setUsed($_token_id)
    {
        $a_query[] = " 
                        UPDATE `tokenHandle` 
                        SET  `status` =  '1' 
                        WHERE `tokenID` =  '{$_token_id}' 
                        LIMIT 1 ;";
        //echo $a_query[0];
        return $this->queryTransaction($a_query);
    }


    function isFree($_token_id)
    {
        if($this->isExist($_token_id))
        {
             // periksa username dan password apakah berada disini
            $sql = "
                        SELECT  status
                        FROM `tokenHandle` 
                        WHERE tokenID=  '{$_token_id}';
                           ";
            $a_hasil =  $this->getResult($sql);
            if(strtotime($a_hasil[0]['status'])  == '0' )
            {
                return true;
            } 
            else
            {
                return false;
            }  
        }
        else
        {
            return false;
        }
    }

    function detail($_token_id)
    {
        if($this->isExist($_token_id))
        {
            $sql = "
                        SELECT  *
                        FROM `tokenHandle` 
                        WHERE tokenID=  '{$_token_id}';
                           ";
            return $this->getResult($sql);
        }
        else
        {
            return false;
        }
    }
}

?>