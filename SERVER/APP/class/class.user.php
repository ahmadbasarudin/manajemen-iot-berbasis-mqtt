<?php
/*
|--------------------------------------------------------------------------
| Userinfo
|--------------------------------------------------------------------------
|
|diperbolehkan siapa saja yang mengkases dengan membandingkan ip address / imei sewaktu login
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class UserInfo extends  masterDB
{
     var  $db_host = DB_HOST, //string
          $db_name =DB_NAME, //string
          $db_user =DB_USER , //string
          $db_password = DB_PASSWORD, // string
          $userTable = "user", //string
          $debug = 0, //debugging boolean
          $userActive,
          $userPrevileges,
          $userList; //array
    /**
    * Constructor
    */
    function __construct()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

    //mengeset untuk mengisi data userinfo berdasarkan username
    function setUsernameActive($_username)
    {
        if(empty($_username))
        {
            $_username = $_SESSION['username'];
        }
        $this->userActive = $_username;
    }

    //get detil per user berdasarkan userName
    function  getUsernameInfo()
    {
        // periksa username dan password apakah berada disini
        return $this->getUserIDByUsername($this->userActive);
    }


    //memberikan nilai  array userID
    function getUserIDByUsername($_username)
    {
        $sql =    "    SELECT userID,lastSession FROM `user`  "
                ."    WHERE username = '{$_username}';   ";
        return   $this->getResult($sql);
    }

    //mendapatkan user detail berdasarkan user ID
    function getUserDetail($_user_id)
    {
        $sql =    " SELECT *
                    FROM user
                    WHERE user.userID = '{$_user_id}';";
        return  $this->getResult($sql);
    }
    function getUserList($_condition,$_order,$_limit)
    {
        // periksa username dan password apakah berada disini
        $sql = "       SELECT * FROM `user` A
                    {$_condition} {$_order} {$_limit} ";
        //print_r($sql);exit();
        return $this->getResult($sql);
    }


    function getUserCount($_condition)
    {
        // periksa username dan password apakah berada disini
        $sql = "        SELECT count(*) as total FROM `user` 
                      {$_condition}  ";
        //echo $sql;
        $a_hasil =  $this->getResult($sql);
        return $a_hasil[0]['total'];
    }

    //dapatkan halaman yang berhak diakses oleh user / pengguna
    function getUserPrivileges($_s_user_id)
    {
        $sql =    " SELECT * FROM vUserAccessLevel ".
                    " where userID='{$_s_user_id}';";
        return $this->getResult($sql);
    }



    //digunakan untuk mengubah password pada profile
    function getUserPassword($_user_id)
    {
        $sql = "SELECT password FROM `user` WHERE userID = '{$_user_id}'";
        return $this->getResult($sql);
    }


    function setUserPassword($_user_id,$_sPasswd)
    {
        $sql = "UPDATE `user` SET `password` =  md5('{$_sPasswd}') WHERE `userID`  = '{$_user_id}'";
        return $this->query($sql);
    }
     
    function setUserPhone($_user_id,$_sPhone)
    {
        $sql = "UPDATE `user` SET `phone` =  '{$_sPhone}' WHERE `userID`  = '{$_user_id}'";
        return $this->setQuery($sql);
    }

    function setAvatar($_user_id,$_sAvatar)
    {
        $sql = "UPDATE `user` SET `avatar` =  '{$_sAvatar}' WHERE `userID`  = '{$_user_id}'";
        return $this->setQuery($sql);
    }

    function deleteUser($_user_id)
    {
        $sql = "UPDATE `user` SET `active` =  '0' WHERE `userID`  = '{$_user_id}'";
        return $this->setQuery($sql);
    }

    function setRequestActivation($_user_id,$_password)
    {
        $sql = "UPDATE `user` SET `active` =  '2',`password` = md5('{$_password}') WHERE `userID`  = '{$_user_id}'";
        return $this->setQuery($sql);
    }


    function setActive($_user_id)
    {
        $sql = "UPDATE `user` SET `active` =  '1' WHERE `userID`  = '{$_user_id}'";
        return $this->query($sql);
    }

    function setNonActive($_user_id)
    {
        $sql = "UPDATE `user` SET `active` =  '2' WHERE `userID`  = '{$_user_id}'";
        return $this->query($sql);
    }

    function getUserByGroup($_s_group)
    {
        $sql = "  SELECT B.*
                FROM  `userGroup` A
                LEFT JOIN user B ON A.`userID` = B.userID
                WHERE  `groupID` LIKE  '{$_s_group}';";
        return $this->getResult($sql);
    }

    function updateSessionLogin($_s_username){
        $a_query[]  =  "    UPDATE `user`  
                            SET `lastSession` = '".session_id()."'".
                            "   ,`lastLogin` = now() ".
                            " WHERE 
                                `username` =  '{$_s_username}'
                            LIMIT 1 ";
        return $this->queryTransaction($a_query);

    }



    function isLoggedIn()
    {
        if(isset($_SESSION['LoggedIn']))
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
    
    /**
    * hancurkan session data/Logout.
    */
    function logout()
    {
        unset($_SESSION['LoggedIn']);
        unset($_SESSION['userName']);
        session_destroy();
    }

    function create(
            $a_hak_akses,
            $s_user_id,
            $s_username,
            $s_password,
            $s_realname,
            $s_mail,
            $s_wilayah)
    {
        $a_query[] = "  INSERT INTO  `user` (`userID` ,   `username` ,`realName` , `password` , `mail` ,  `wilayahPenempatan` ,  `active`)
                      VALUES ('{$s_user_id}',  '{$s_username}', '{$s_realname}',  md5('$s_password'), '{$s_mail}' ,    '{$s_wilayah}',   '1')";
        if(isset($a_hak_akses))
        {
            if(count($a_hak_akses)>0)
            {
                for($i=0;$i<count($a_hak_akses);$i++)
                {
                    if($a_hak_akses[$i] != "GRP00000000000000000")
                    {
                        $a_query[] = "INSERT INTO `userGroup`  ( `userID`, `groupID`) VALUES ( '{$s_user_id}', '{$a_hak_akses[$i]}');";
                    }
                }
            }
        }
        $a_query[] = "INSERT INTO `userGroup`  ( `userID`, `groupID`) VALUES ( '{$s_user_id}', 'GRP00000000000000000');";
        //print_r($a_query); exit();
        return $this->queryTransaction($a_query);
    }
    function getWilayahKerja($_condition,$_order,$_limit)
    {
        // periksa username dan password apakah berada disini
        $sql = "       SELECT * FROM `wilayahKerja` A
                    {$_condition} {$_order} {$_limit} ";
        return $this->getResult($sql);
    }


    function setTheme($_user_id,$_theme)
    {
        $theme = $this->antiInjection($_theme);
        $sql = "UPDATE `user` SET `theme` =  '{$theme}' WHERE `userID`  = '{$_user_id}'";
        return $this->query($sql);
    }

    function setPhoto($_user_id,$_photo_path)
    {
        $user_id = $this->antiInjection($_user_id);
        $photo_path = $this->antiInjection($_photo_path);
        $sql = "UPDATE `user` SET `avatar` =  '{$photo_path}' WHERE `userID`  = '{$user_id}'";
        return $this->query($sql);
    }
}

?>