<?php

/*
|--------------------------------------------------------------------------
| Firewall
|--------------------------------------------------------------------------
|
|diperbolehkan siapa saja yang mengkases dengan membandingkan ip address / imei sewaktu login
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
|    
*/

class MasterDB
{

    protected $dbHandler_i;       // for mysqli handler
    protected $result;
    protected $debug=false;   // true: enable    false: disable

    private $name;
    private $host;
    private $user;
    private $password;
    private $formatData = "ASSOC"; //OBJECT / ARRAY / ASSOC

    public $sql = "";


    public function __construct($_user, $_pass, $_name, $_host)
    {
        $this->name =$_name;
        $this->host = $_host;
        $this->user = $_user;
        $this->password  = $_pass;
        $this->connectDB();
    }


    private function connectDB()
    {
        $this->dbHandler_i=mysqli_connect($this->host,$this->user,$this->password);
        if (!$this->dbHandler_i)
        {
            die("Tidak dapat tersambung  DB ".$this->name.". periksa database di ".$this->host."<br />");
            return 0;
        }
        else
        {
            if ($this->debug)
            {
                echo "<BR />Database Tersambung.\n";
            }
        }
        if (mysqli_select_db($this->getdbHandler(),$this->name))
        {
            if($this->debug)
            {
                echo "<BR />Database {$this->name} ditemukan.\n";
            }
            return 1;
        }
        else
        {
            if ($this->debug)
            {
            echo "<BR />Database {$this->name} tidak ditemukan di {$this->host}\n";
            }
            return 0;
        }
    }



    public  function getdbHandler()
    {
        if (!$this->dbHandler_i)
        {
            $this->connectDB();
        }
        return $this->dbHandler_i;
    }



    public function closeDB()
    {
        return mysqli_close($this->dbHandler_i);
    }



    public function antiInjection($_string)
    {
        $string = mysqli_real_escape_string($this->getdbHandler(),stripslashes(strip_tags(htmlspecialchars($_string,ENT_QUOTES))));
        return $string;
    }

    public function lastInsertID()
    {
        return mysqli_insert_id($this->dbHandler_i);
    }


    public function getResult($_query)
    {
        if($this->query($_query))
        {
            $i=0;
            $this->result = mysqli_use_result($this->getdbHandler());
            if($this->result)
            {
                if($this->formatData == "OBJECT")
                {
                    while ( $row = @mysqli_fetch_object($this->result))
                    {
                        //menghasilkan nilai dari object
                        $record[$i] = $row;
                        $i++;
                    }
                }
                elseif($this->formatData == "ARRAY")
                {
                    while ( $row = @mysqli_fetch_array($this->result,MYSQLI_NUM))
                    {
                        //menghasilkan nilai dari object
                        $record[$i] = $row;
                        $i++;
                    }
                }
                elseif($this->formatData == "ASSOC")
                {
                    while ( $row = @mysqli_fetch_assoc($this->result))
                    {
                        //menghasilkan nilai dari object
                        $record[$i] = $row;
                        $i++;
                    }
                }
                mysqli_free_result($this->result);
                if ($this->debug)
                {
                    echo "<BR />Hasil fecth mysql dikeluarkan.\n";
                }
                if(isset($record))
                {
                    return $record;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if ($this->debug)
                {
                    echo "<BR />Hasil dari query error.\n";
                    echo "<BR />{$_query} \n";
                }
                return false;
            }
        }
        else
        {
            if ($this->debug)
            {
                echo "<BR />Query yang dimasukan  error.\n";
            }
            return false;
        }
    }


    //transaction
    public function  autocommit($b_bool)
    {
        return mysqli_autocommit($this->dbHandler_i,$b_bool);
    }

    public function query($s_query)
    {
        return mysqli_real_query($this->dbHandler_i,$s_query);
    }

    public function commit()
    {
        return mysqli_commit($this->dbHandler_i);
    }

    public function rollback()
    {
        return mysqli_rollback($this->dbHandler_i);
    }

    public function queryTransaction($a_query)
    {
        $this->autocommit(FALSE);
        $b_result = true;
        foreach ($a_query as $s_query) 
        {
            if(!$this->query($s_query))
            {
                $b_result = false;
            }
        }
        if($b_result)
        {
            $this->commit();
            $this->autocommit(TRUE);
            return true;
        }
        else
        {
            $this->rollback();
            $this->autocommit(TRUE);
            return false;
        }
    }

    public function sqlInsert($_table,$_array)
    {

        foreach ($_array as $key => $value) 
        {
            $column[]   = $key;
            if(isset($value))
            {
                if($value == null)
                {

                    $cell[] = "null";
                }
                else
                {
                    $cell[]     = "\"".$value."\"";
                }
            }
            else
            {
                $cell[] = "null";
            }
        }

        $s_field    =  "(".implode(",", $column).")";
        $s_data     =  "(" . implode(",", $cell) . ")";

        $sql    =   "   INSERT INTO `{$_table}` {$s_field} 
                        VALUES {$s_data}; ";
        return $sql;
                
    }
}
?>