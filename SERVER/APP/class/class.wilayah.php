<?php
/*
|--------------------------------------------------------------------------
| wilayah
|--------------------------------------------------------------------------
|
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
	class Wilayah extends  masterDB
	{
	    /**
	    * Constructor
	    */
	    function __construct()
	    {
	        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
	    }

		/**
		* periksa list wilayah yang ada didatabase
		*
		* @return array list wilayah
		*/
		function getList($_condition,$_order,$_limit)
		{
			$sql =    "    	SELECT child.*,parent.namaWilayah as namaParent
							FROM `wilayahKerja`  child
							LEFT JOIN `wilayahKerja` parent on parent.idWilayah = child.parentWilayah
							{$_condition}  {$_order} {$_limit} 

			" ;
			return $this->getResult($sql);
		}
		
		

	    function getCount($_condition)
	    {
	        $sql =  "   SELECT count(*) as total 
	                    FROM `wilayahKerja` 
	                    {$_condition} ";
	        $a_hasil =  $this->getResult($sql);
	        return $a_hasil[0]['total'];

	    }
	    function delete($_wilayah_id)
	    {
	    	$_wilayah_id = $this->antiInjection($_wilayah_id);
        	$a_query[] =  "
        						DELETE FROM `wilayahKerja` 
        						WHERE `idWilayah` = '{$_wilayah_id}'" ;

        	return $this->queryTransaction($a_query); 
	    }
	    function create($a_wilayah)
	    {
	    	//key :idWilayah,namaWilayah,parentWilayah
	    	if($a_wilayah['parentWilayah'] == "")
	    	{
	    		$a_wilayah['parentWilayah'] = " null ";	
	    	}
	    	else
	    	{
	    		$a_wilayah['parentWilayah'] = "'".$a_wilayah['parentWilayah']."'";
	    	}

	    	$a_query[] = " 	INSERT INTO `wilayahKerja` 
	    					(`idWilayah`, `namaWilayah`, `parentWilayah`) 
	    					VALUES ('{$a_wilayah['idWilayah']}', '{$a_wilayah['namaWilayah']}', {$a_wilayah['parentWilayah']}) ";
        	return $this->queryTransaction($a_query); 
	    }
	}
?>