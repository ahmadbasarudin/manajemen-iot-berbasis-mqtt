<?php


/*
|--------------------------------------------------------------------------
| Notification
|--------------------------------------------------------------------------
|
|class notifikasi berfungsi untuk memanajemen tabel notifikasi
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
|    
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Notification extends  masterDB
{
     var  $debug = 0; //array
     /**
     * Constructor
     */
     function __construct()
     {
          parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
     }

     function getList($_condition,$_order,$_limit)
     {
          $sql =    "    SELECT *
                         FROM `notification` 
                         {$_condition}  {$_order} {$_limit} 

                          " ;
          return $this->getResult($sql);
     }

     function getCount($_condition)
     {
          $sql =    "    SELECT count(*) as total 
                         FROM `notification` 
                         {$_condition} ";
          $aHasil =  $this->getResult($sql);
          return $aHasil[0]['total'];
     }

     function delete($_notifID)
     {
          $a_query[] =    "    UPDATE  `notification` SET  `notifStatus` =  '9' 
                         WHERE  `notification`.`notifID` ={$_notifID}; ";
          return $this->queryTransaction($a_query);
     }

     function update($_notifID,$_flag)
     {
          $a_query[] =    "    UPDATE  `notification` SET  `notifStatus` =  '{$_flag}' 
                         WHERE  `notification`.`notifID` ={$_notifID}; ";
          return $this->queryTransaction($a_query);
     }

     function create(
                    $_notifTitle ,
                    $_notifMessage ,
                    $_notifLink ,
                    $_notifUserTo ,
                    $_notifToFirebaseID ,
                    $_notifUserFrom ,
                    $_notifLogo 
               )
     {
          $_notifMessage = str_replace('"', '`', $_notifMessage);
          $_notifMessage = str_replace("'", '`', $_notifMessage);
          $_notifMessage = $this->antiInjection($_notifMessage);
          $dt_now = date('Y-m-d H:i:s');
          $a_query[] =    "
                         INSERT INTO `notification` (
                              `notifTitle` ,
                              `notifMessage` ,
                              `notifLink` ,
                              `notifUserTo` ,
                              `notifToFirebaseID` ,
                              `notifUserFrom` ,
                              `notifDateCreate` ,
                              `notifLogo` 
                         )
                         VALUES (
                          '$_notifTitle', '$_notifMessage', '$_notifLink', '$_notifUserTo', '$_notifToFirebaseID', '$_notifUserFrom','$dt_now' , '$_notifLogo'
                         );
                    ";
          //echo $a_query[0];
          return $this->queryTransaction($a_query);
     }
}
?>