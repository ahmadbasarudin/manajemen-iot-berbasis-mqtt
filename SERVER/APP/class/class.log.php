<?php
/*
|--------------------------------------------------------------------------
|Logging
|--------------------------------------------------------------------------
|logging dan mendapatkan list data dari database log
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
|    
*/
include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Log extends  MasterDB
{
     var  $db_host = DB_HOST,
          $db_name =DB_NAME,
          $db_user =DB_USER ,
          $db_password = DB_PASSWORD,
          $log_user_id = "",
          $log_modul_id = "",
          $log_detail = "",
          $debug = false; 


     /**
          * Constructor untuk koneksi ke database
     **/
     function __construct()
     {
          parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
     }
     //mencatat di database
     function logging($_log_user_id,$_user_agent,$_client_address,$_url_request,$_modul_id,$_log_detail)
     {
          $sql = "SELECT getNextID('log',true) as logID;";
          $tmpID = $this->getResult($sql);
          if(isset($tmpID[0]['logID']))
          {
               $sql = "INSERT INTO `log` (`logID`, `logUserID`, `logUserAgent`, `logTime`, `logClientIP`,`logClientURL`, `logModulID`, `logDetail`) VALUES ('{$tmpID[0]['logID']}', '{$_log_user_id}', '{$_user_agent}', now(),  '{$_client_address}','{$_url_request}', '{$_modul_id}', '{$_log_detail}');";
               
               if($this->query($sql))
               {
                    return true;
               }
               else
               {
                    return false;
               }
          }
          else
          {
               return false;
          }
     }
     
     function getList($_sCondition,$_sOrder,$_sLimit)
     {
          $sql =    "    SELECT * FROM log {$_sCondition} {$_sOrder} {$_sLimit}";
          return $this->getResult($sql);
     }

}

?>
