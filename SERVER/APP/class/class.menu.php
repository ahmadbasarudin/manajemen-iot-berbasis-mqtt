<?php

/*
|--------------------------------------------------------------------------
| Menu
|--------------------------------------------------------------------------
|
|mendapatkan data menu dari database
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
|    
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class MenuLinks extends  masterDB
{
	/**
	* Constructor untuk koneksi ke database
	*/
	function __construct()
	{
		parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
	}

    function childCount($_menu_id,$_user_id)
    {
        $sql = "   	SELECT count(*) as total FROM `vUserMenu` 
                	WHERE parentID = '{$_menu_id}' AND userID = '{$_user_id}' ; ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }

	function buildMenu($_user_id)
	{

        //class default icon
        $s_sidebar_class_icon  = " fas fa fa-circle ";
		$s_html_menu_container = "";
		$s_html_menu_main = "";
		$s_html_menu_child = "";
		$sql =    	" SELECT * ".
					" FROM vUserMenu ".
					" WHERE userID = '{$_user_id}' AND (parentID is null OR parentID = 0)  AND status=1 ".
					" group by `id`".
					" ORDER BY  `vUserMenu`.`menuOrder` ASC  ";
      	$a_main_menu =  $this->getResult($sql);
        if(isset($a_main_menu))
        {
            foreach ($a_main_menu as $key => $value) 
            {
                if($a_main_menu[$key]['icon'] == "")
                {
                    $a_main_menu[$key]['icon'] = $s_sidebar_class_icon ;
                }
                $s_html_menu_child = "";
                $s_carret = "";
                if($this->childCount($a_main_menu[$key]['id'],$_user_id) > 0)
                {

                    $s_html_menu_main_link = "<a data-toggle='collapse'  href='#{$a_main_menu[$key]['id']}'>";
                    $s_carret = "<span class='caret'></span>";

                    $a_child_menu = $this->getChildMenu($_user_id,$a_main_menu[$key]['id']); 
                    if(isset($a_child_menu))
                    {
                                   
                        $s_html_menu_child =    "
                                                <div class='collapse' id='{$a_main_menu[$key]['id']}'>
                                                    <ul class='nav nav-collapse'>
                                            ";
                        foreach ($a_child_menu as $key_child => $value_child) 
                        {
                            if($a_child_menu[$key_child]['icon'] == "")
                            {
                                $a_child_menu[$key_child]['icon'] = $s_sidebar_class_icon;
                            }
                            $s_html_menu_child .=   "
                                                            <li>
                                                                <a href='{$a_child_menu[$key_child]['url']}'>

                                                                    <span  class='sub-item'>{$a_child_menu[$key_child]['title']}</span>
                                                                    
                                                                </a>
                                                            </li>
                                                    ";

                        }                                    

                        $s_html_menu_child .=   "
                                                    </ul>
                                                </div>
                                        ";
                    }  
                }
                else
                {
                    $s_html_menu_main_link = "<a href='{$a_main_menu[$key]['url']}'>";
                }
                $s_html_menu_main .= "
                                        <li class='nav-item'>
                                            {$s_html_menu_main_link}
                                                <i class='{$a_main_menu[$key]['icon']}'></i>
                                                <p>{$a_main_menu[$key]['title']}</p>
                                                $s_carret
                                            </a>
                                            {$s_html_menu_child}

                                        </li>
                                ";
                //periksa apakah menu ini mempunyai anak?
                //jika punya anak maka tetap tampilkan nilai menu tetapi link nya tidak

                
            }
        }
          	
      	$s_html_menu_container .= "
		                    <ul class='nav nav-primary'>
                                {$s_html_menu_main}
                                
                                        
                                <li class='mx-4 mt-2'>
                                    <a href='index.php?page=logout' class='btn btn-danger btn-block'><span class='btn-label mr-2'> <i class='fa fa-power-off'></i> </span>Logout</a> 
                                </li>
	                        </ul>
                        ";

        return $s_html_menu_container ;
	}


	function getChildMenu($_s_user_id,$_s_parent_id)
	{
		$sql =    	" SELECT * ".
					" FROM vUserMenu ".
					" WHERE userID = '{$_s_user_id}' AND parentID = '{$_s_parent_id}' AND status=1 ".
					" group by id".
					" ORDER BY  `vUserMenu`.`menuOrder` ASC  ";
		return $this->getResult($sql);
	}


    /**
    * rendering menu dari array $_a_data
    * int $_i_parent sebagai induk dari menu dengan default 0
    *
    **/
    function getBuildMenu($_a_data, $_i_parent = 0) 
    {
		/*
        echo "<pre>";
        print_r($_a_data);
        echo "</pre>";
        exit;
        */
        static $i = 1;
        if (isset($_a_data[$_i_parent])) 
        {
            if($_i_parent != 0)
            {
                $html = "
                        <ul class='sidebar-dropdown' >";
            }
            else
            {
                /*
                echo "<pre>";
                print_r($_a_data);
                echo "</pre>";
                echo $_i_parent."||".$_a_data[$_i_parent][0]['title'];exit;
                */
                $html = " ";
            }
            $i++;
            foreach ($_a_data[$_i_parent] as $v) 
            {
                //recursive
                $child = $this->getBuildMenu($_a_data, $v['id']);
                if($_i_parent == 0)
                {
                    if(isset($v['url']) && $v['url'] !="")
                    {
                        $html .= "
                                <li >
                                    <a href='".$v['url']."'  >{$v['title']}</a>";
                    }
                    else
                    {
                        $html .= "
                                <li class='sidebar-dropdown'>
                                    <a href='javascript:;'  >{$v['title']}<i class='fa fa-angle-down'></i></a>";

                    }
                }
                else
                {
                    if(isset($v['url']) && $v['url'] !="")
                    {
                        $html .= "
                                <li >
                                    <a href='".$v['url']."'  >{$v['title']}</a>";
                    }
                    else
                    {
                        $html .= "
                                <li class='dropdown-submenu'>
                                    <a href='#'  >{$v['title']}</a>";
                    }
                }
                if ($child) 
                {
                    $i--;
                    $html .= $child;
                }
                $html .= '
                        </li>';
            }
            if($_i_parent == 0)
            {
                $html .= "";
            }
            else
            {
                $html .= "
                    </ul>";
            }
            return $html;
        }
        else 
        {
            return false;
        }
    }
}
?>