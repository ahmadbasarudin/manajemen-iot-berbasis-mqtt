<?php
/*
|--------------------------------------------------------------------------
| Digiflaszz
|--------------------------------------------------------------------------
|
|menghandle device android yang digunakan  class digiflazz
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Digiflazz extends  masterDB
{

    
    private $_head;
    private $_url;
    private $_username;
    private $_key;

    /**
    * Constructor
    */
    function __construct()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);

        $this->_head = array(
            'Content-Type:application/json'
        );
        $this->_url = DIGIFLAZZ_URL_TRANSCATION;
        $this->_username = DIGIFLAZZ_USERNAME;
        $this->_key = DIGIFLAZZ_KEY;
    }

    private function _request($_url, $_head, $_data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $_head);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $_data);
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        // curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $result = curl_exec($ch);

        return $result;
    }


    public function getStatusPLN($_transaksi_id, $_customer_no, $_sku_code) {
        $sign = $this->_username.$this->_key.$_transaksi_id;

        $data = json_encode( array(
            "commands"=> "inq-pasca",
            'username'=> $this->_username,
            'buyer_sku_code'=> $_sku_code,
            'customer_no'=> $_customer_no,
            'ref_id'=> $_transaksi_id,
            'sign'=> md5($sign)
        ) );
        //print_r($data);

        $exec = $this->_request($this->_url, $this->_head, $data);
        return $exec;
    }
}

?>