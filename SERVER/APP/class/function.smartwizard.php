<?php
/*
|--------------------------------------------------------------------------
| smart wizard
|--------------------------------------------------------------------------
|
|menampilkan step saja dengan input array dan ouput string
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
	//harus include css smart wizard dan theme arrow
    //input array {status,header,footer} ->status = active atau kosong( "" )

	/*
	contoh : 
	
    $a_step_wizard[0]['status'] = "done";
    $a_step_wizard[0]['header'] = "TAHAP 1";
    $a_step_wizard[0]['footer'] = "Jadwal";

    $a_step_wizard[1]['status'] = "active";
    $a_step_wizard[1]['header'] = "TAHAP 2";
    $a_step_wizard[1]['footer'] = "Pekerjaan";

    $a_step_wizard[2]['status'] = "";
    $a_step_wizard[2]['header'] = "TAHAP 3";
    $a_step_wizard[2]['footer'] = "PIC";
    */

	function buildSmartWizard($_a_step)
	{
		$s_output = "";
		if(isset($_a_step))
		{
			if(count($_a_step))
			{

		        $s_output = "
		                <div id='smartwizard' class='sw-main sw-theme-arrows'>
		                    <ul class='nav nav-tabs step-anchor'>";
				for($i=0;$i<count($_a_step);$i++)
				{
					$s_output .= "<li class='{$_a_step[$i]['status']}'><a >{$_a_step[$i]['header']}<br /><small>{$_a_step[$i]['footer']}</small></a></li>";

				}
				$s_output .= "
		                    </ul>
		                </div>";
			}
			else
			{
				$s_output = "jumlah parameter harus lebih dari 1";
			}
		}
		else
		{
			$s_output = "parameter step tidak ada";
		}

		return $s_output;
	}
?>