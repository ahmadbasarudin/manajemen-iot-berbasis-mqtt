<?php
/*
|--------------------------------------------------------------------------
| Config
|--------------------------------------------------------------------------
|
|Config system
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    $APP =  "DEV"; //DEV atau PRO
    
    if($APP == "DEV") //develop
    {  
        //------------------------------------------------
        DEFINE("DB_USER" , "root");
        DEFINE("DB_PASSWORD" ,"");
        DEFINE("DB_NAME" ,"iot_mqtt");
        DEFINE("DB_HOST" , "localhost");
        $SYSTEM['DIR_PATH'] = "/Users/basar/Desktop/manajemen-iot-berbasis-mqtt/SERVER/APP";
        //------------------------------------------------
    }

    else if($APP == "PRO" ) // production
    {
        //------------------------------------------------
        DEFINE("DB_USER" , "u3966337_bossman");
        DEFINE("DB_PASSWORD" ,"b055m4n");
        DEFINE("DB_NAME" ,"u3966337_matrik");
        DEFINE("DB_HOST" , "localhost");
        $SYSTEM['DIR_PATH'] = "/home/u3966337/public_html/sistem-manajemen-mqtt-device/SERVER/APP";
        //------------------------------------------------
    }
    
    $SYSTEM['DIR_MODUL']                = $SYSTEM['DIR_PATH'] ."/modul";
    $SYSTEM['DIR_MODUL_CLASS']          = $SYSTEM['DIR_PATH'] ."/class";
    $SYSTEM['DIR_MODUL_CORE']           = $SYSTEM['DIR_MODUL'] ."/core";
    $SYSTEM['DIR_MODUL_LAYOUT']         = $SYSTEM['DIR_MODUL'] ."/layout";
    $SYSTEM['DIR_MODUL_UPLOAD']         = $SYSTEM['DIR_PATH'] ."/uploaded";

    DEFINE("SUPER_ADMIN_GROUP","SUPER_ADMINISTRATOR");
    DEFINE("LOGIN_REFERENCE","PASSWORD");
    $SYSTEM['APP_NAME']                 = "MCU Management"; 
    //skin-dark 
    //modul skin terletak di css/skins
    $SYSTEM['SKIN']                     ='  skin-dark '; //OFF, ON
    $SYSTEM['LOGO']                     = "";
    $SYSTEM['FIREWALL']                 = 'OFF'; //OFF, ON

    //AUDIT KOMPUTER
    //untuk config detail silahkan edit file di modul audit audit.config.php
    $AUDIT['KEEP_CONFIG']               = 'y'; //y or n. file config tidak dihapus setelah eksekusi



    //FIREBASE
    $SYSTEM['FIREBASE_DELAY']           = 40; //40detik
    $SYSTEM['FIREBASE_FLAG_SEND']       = 1;
    $SYSTEM['FIREBASE_FLAG_RECEIVED']   = 2;
    $SYSTEM['FIREBASE_FLAG_READ']       = 3;
    $SYSTEM['FIREBASE_FLAG_ERROR']      = 8;
    $SYSTEM['FIREBASE_FLAG_DELETE']     = 9;
    $SYSTEM['FIREBASE_URL']             = 'https://fcm.googleapis.com/fcm/send';
    $SYSTEM['FIREBASE_HEADER']          = array(
                                                'Authorization:key = AIzaSyANaMm08VOerp97jR6Kxaru73EyAAj_HdE ',
                                                'Content-Type: application/json'
                                            );


    //DIGIFLAZZ
    DEFINE("DIGIFLAZZ_URL_TRANSCATION" , "https://api.digiflazz.com/v1/transaction");
    DEFINE("DIGIFLAZZ_USERNAME" , "pexevaD8Pp5D");
    DEFINE("DIGIFLAZZ_KEY" ,"b464a9a2-fa8a-50d3-a9c8-b15eba37cda2");
    DEFINE("DIGIFLAZZ_SKU_PLN" ,"pln");


    //MQTT BROKER
    //untuk cron penyimpanan data log
    DEFINE("MQTT_SERVER" , "broker.emqx.io");
    DEFINE("MQTT_PORT" , 1883);
    DEFINE("MQTT_USERNAME" , "");
    DEFINE("MQTT_PASSWORD" , "");
    DEFINE("MQTT_CLIENT_ID" , "MATRIX_SERVER_CROND");



?>