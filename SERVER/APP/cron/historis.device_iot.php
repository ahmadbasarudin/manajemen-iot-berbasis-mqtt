<?php
    /*
    |--------------------------------------------------------------------------
    | Menyimpan data log historis dari mqtt iot device
    |--------------------------------------------------------------------------
    |
    |
    |diperbolehkan siapa saja yang mengakses dengan membandingkan ip address / imei sewaktu login
	|Digunakan untuk membuat log
	|prefix parameter pada class:
	|     _ :  parameter 
	|     i :  integer 
	|     b :  boolean 
	|     a :  array 
	|     s :  string
    */
	
    include_once("../config.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.device_iot.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.mqtt.php");

    $oMqtt = new Bluerhinos\phpMQTT(MQTT_SERVER, MQTT_PORT, MQTT_CLIENT_ID);
    if(!$oMqtt->connect(true, NULL, MQTT_USERNAME, MQTT_CLIENT_ID)) {
        exit(1);
    }

    //$oMqtt->debug = true;
    $oMqtt->debug = false;

    $topics['DEVICE/APP_MATRIK/CROND'] = array('qos' => 0, 'function' => 'procMsg');
    $oMqtt->subscribe($topics, 0);

    while($oMqtt->proc()) {

    }

    $oMqtt->close();

    function procMsg($topic, $_msg){
            echo 'Msg Recieved: ' . date('r') . "\n";
            echo "Topic: {$topic}\n\n";
            echo "\t$_msg\n\n";



            //{"client-id":"EC:FA:BC:5E:DC:A9","state":"{"MCU":{"RELAY":"OFF","PIN1":"0","PIN2":"1"}}"}
            $data = json_decode($_msg,true);
            if (json_last_error() == JSON_ERROR_NONE)
            {

                $input['diID']          = $data['client-id'];
                $input['dihDesc']       = $data['MCU']['RELAY'];
                $input['dihMessage']    = "";

                $oDeviceIot = new DeviceIot();

                echo '[' . date('r') . "][SYSTEM]";
                if($oDeviceIot->historyAdd($input))
                {
                    echo "data berhasil dicatat di sistem \n \n";
                }
                else
                {

                    echo "data berhasil dicatat di sistem";
                }
                $oDeviceIot->closeDB();
            }
            else
            {
                    echo "parsing json error";

            }
    }


?>