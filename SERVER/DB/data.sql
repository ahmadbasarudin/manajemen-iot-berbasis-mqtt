-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 04, 2020 at 08:08 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iot_mqtt`
--
CREATE DATABASE IF NOT EXISTS `iot_mqtt` ;
USE `iot_mqtt`;

--
-- Dumping data for table `appVersion`
--

INSERT INTO `appVersion` (`appName`, `lastVersion`) VALUES
('simple_app', 1),
('simple_client', 1),
('com.basar.administrator', 1);

INSERT INTO `mcuCategory` (`mcuCatID`, `mcuName`) VALUES
(NULL, 'ESP 32'),
(NULL, 'ESP 8266'),
(NULL, 'STM 32'),
(NULL, 'RASPBERRY PI'),
(NULL, 'ORANGE PI');

--
-- Dumping data for table `counterDB`
--

INSERT INTO `counterDB` (`tableName`, `initial`, `lastUpdate`, `nextID`, `resetID`, `notes`) VALUES
('log', 'LOG', '2020-02-02', 2, 1, NULL),
('user', 'USER', '2012-10-15', 2, 1, NULL);

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`groupID`, `name`, `image`, `active`, `notes`) VALUES
('GRP00000000000000000', 'COMMON', 'assets/img/profile2.jpg', 1, 'common user'),
/*('OWNER_DEVICE', 'PEMILIK DEVICE', 'assets/img/profile.jpg', 1, 'orang yang membeli dan memiliki perangkat IoT'),*/
('SUPER_ADMINISTRATOR', 'Administrator', 'assets/img/profile.jpg', 1, 'mengurusi pengguna berserta group akses level');

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parentID`, `icon`, `title`, `url`, `menuOrder`, `keterangan`, `status`) VALUES
(1, 0, 'fa fa-cubes', 'MASTER', '', 1, NULL, 1),
(2, 1, 'fa fa-home', 'WILAYAH KERJA', 'index.php?page=wilayah', 0, NULL, 1),
(3, 1, 'fa fa-user', 'PENGGUNA', 'index.php?page=user', 1, NULL, 1),
(4, 0, 'fa fa-tablet', 'MOBILE PHONE', 'index.php?page=phone', 3, NULL, 1),
(5, 0, 'fa fas fa-microchip', 'MCU', 'index.php?page=mcu', 3, NULL, 1),
/*(6, 0, 'fa fas  fa-home', 'PELANGGAN PASCA', 'index.php?page=pelangganpascabayar', 3, NULL, 1),*/
(99, 0, 'fas fa-key', 'GANTI PASSWORD', 'index.php?page=user&action=ganti_password', 99, NULL, 1);

--
-- Dumping data for table `pageRegistered`
--

INSERT INTO `pageRegistered` (`pageID`, `filename`, `notes`) VALUES

('IND100', 'index.php', ''),

('UDR101', 'device_register.php', ''),
('UDR102', 'device_register.view.php', ''),
('UDR103', 'device_register.list.php', ''),

('UDR111', 'device_register aktivasi', ''),
('UDR112', 'device_register testnotif', ''),
('UDR113', 'device_register delete', ''),
('UDR114', 'device_register register', ''),

('MDE100', 'mode', ''),
('MCU100', 'mcu', ''),

('NTF100', 'notification', ''),



('USR101', 'user.php', ''),
('USR102', 'user.view.php', ''),
('USR103', 'user.list.php', ''),
('USR104', 'user.create.php', ''),
('USR105', 'user.privileges.php', ''),
('USR106', 'user.aktivasi.php', ''),
('USR111', 'user.upload.svr.php', ''),
('USR112', 'user.password.php', ''),
('USR113', 'user.photo.php', ''),
('USR114', 'user.pengajuan.php', ''),

/*('PPB100', 'pelanggan_pascabayar.php', ''),*/

('WIL100', 'wilayah.model.php', '');


--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `wilayahPenempatan`, `username`, `realName`, `lastLoginIP`, `password`, `mail`, `phone`, `join`, `lastLogin`, `lastSession`, `theme`, `gender`, `avatar`, `notes`,  `active`) VALUES

('basar', '51100', 'basar', 'ahmad basarudin', NULL, '271989bd84d694a533692a6227c43a4d',  NULL, NULL, NULL, '2020-02-02 17:58:50', '98d4349e69b40723697e2c6f99cdf1bf',   'light', 'm', 'uploaded/image/basar_728f7b6707798cad656be6b8d735497c.jpg', NULL, 1);
--
-- Dumping data for table `userAccessLevel`
--

INSERT INTO `userAccessLevel` (`id`, `groupID`, `pageID`, `active`, `notes`) VALUES

(null, 'GRP00000000000000000', 'IND100', 1, NULL),

(null, 'SUPER_ADMINISTRATOR', 'UDR101', 1, NULL),
(null, 'GRP00000000000000000', 'UDR102', 1, NULL),
(null, 'SUPER_ADMINISTRATOR', 'UDR103', 1, NULL),

(null, 'SUPER_ADMINISTRATOR', 'UDR111', 1, NULL),
(null, 'SUPER_ADMINISTRATOR', 'UDR112', 1, NULL),
(null, 'SUPER_ADMINISTRATOR', 'UDR113', 1, NULL),
(null, 'GRP00000000000000000', 'UDR114', 1, NULL),



(null, 'GRP00000000000000000', 'MDE100', 1, ''),
(null, 'GRP00000000000000000', 'NTF100', 1, ''),

(null, 'GRP00000000000000000', 'USR101', 1, ''),
(null, 'GRP00000000000000000', 'USR102', 1, ''),
(null, 'SUPER_ADMINISTRATOR', 'USR103', 1, ''),
(null, 'SUPER_ADMINISTRATOR', 'USR104', 1, ''),
(null, 'SUPER_ADMINISTRATOR', 'USR105', 1, ''),
(null, 'SUPER_ADMINISTRATOR', 'USR106', 1, ''),
(null, 'GRP00000000000000000', 'USR111', 1, ''),
(null, 'GRP00000000000000000', 'USR112', 1, ''),
(null, 'GRP00000000000000000', 'USR113', 1, ''),
(null, 'SUPER_ADMINISTRATOR', 'USR114', 1, ''),

(null, 'SUPER_ADMINISTRATOR', 'WIL100', 1, ''),

(null, 'SUPER_ADMINISTRATOR', 'MCU100', 1, '');

/*(null, 'OWNER_DEVICE', 'PPB100', 1, '');*/

--
-- Dumping data for table `userGroup`
--

INSERT INTO `userGroup` (`userGroupID`, `userID`, `groupID`, `status`) VALUES
(null, 'basar', 'SUPER_ADMINISTRATOR', 1),
(null, 'basar', 'GRP00000000000000000', 1);

--
-- Dumping data for table `userMenu`
--

INSERT INTO `userMenu` (`userMenuID`, `menuID`, `groupID`, `status`) VALUES
(null, 1, 'SUPER_ADMINISTRATOR', 1),
(null, 2, 'SUPER_ADMINISTRATOR', 1),
(null, 3, 'SUPER_ADMINISTRATOR', 1),
(null, 4, 'SUPER_ADMINISTRATOR', 1),
(null, 5, 'SUPER_ADMINISTRATOR', 1),
/*(null, 6, 'OWNER_DEVICE', 1),*/
(null, 99, 'SUPER_ADMINISTRATOR', 1);

--
-- Dumping data for table `wilayahKerja`
--

INSERT INTO `wilayahKerja` (`idWilayah`, `namaWilayah`, `parentWilayah`, `kontakWilayah`) VALUES
('51100', 'AREA SURABAYA UTARA', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
