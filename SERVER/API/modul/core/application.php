<?php
/*
|--------------------------------------------------------------------------
| Application Model
|--------------------------------------------------------------------------
|
|Model application
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	$PAGE_ID="APPLICATION";
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.app.php");
	//require_once($SYSTEM['DIR_PATH']."/class/class.app.php");
	$oApp = new Application();
	//jika variable error sudah terisi maka kosongkanlah
	$response['status'] = "error";
	$response['desc'] = "tidak tereksekusi";

	$sError  = "";
    $a_errors = array();

	if(isset($_REQUEST['action']))
	{
		if($_REQUEST['action'] == "version")
		{
			if(!isset($_REQUEST['appName']))
            {
                $a_errors[] = "ID applikasi tidak ada";
            }

            if (!$a_errors) 
            {
            	//cek apakah ada di pengguna
                $s_where = " WHERE appName = '{$_REQUEST['appName']}' ";
                if($oApp->getCount($s_where)> 0)
                {                    
                    $a_data = $oApp->getList($s_where, "", "");
                    $response['status'] = 'success';
                    $response['desc'] = "";
                    $response['version'] = $oApp->getVersion($s_where);	                	
                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "app id tidak ditemukan";
                }
            }
		}
	}

    if ($a_errors) 
    {
    	$sError =  '';
        foreach ($a_errors as $error) 
        {
            $sError .= "$error<br />";
        }
        $response['status'] = 'error';
        $response['desc'] = $sError;
    }
	$oApp->closeDB();

?>