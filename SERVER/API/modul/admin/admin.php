<?php
/*
|--------------------------------------------------------------------------
| Admin
|--------------------------------------------------------------------------
|
|Controler admin
|    
|Digunakan untuk membuat controler admin
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/


	$PAGE_ID="USR106"; //user aktivasi termasuk didalamnya data pengguna
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
	//jika ditemukan error dengan nilai id
	if(isset($_REQUEST['type']))
	{
		if ($_REQUEST['type'] == "model") 
		{
    		require_once($SYSTEM['DIR_MODUL']."/admin/admin.model.php");
	 	} 
	}
?>