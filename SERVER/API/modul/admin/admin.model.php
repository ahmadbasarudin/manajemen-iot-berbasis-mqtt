<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
    
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
    $oUser = new UserInfo();
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.group.php");
    $oGroup = new Group();

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.device.php");
    $oDevice = new Device();

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.notification.php");
    $oNotif = new Notification();

    require_once($SYSTEM['DIR_MODUL_CLASS']."/function.firebase.php");

    //jika variable error sudah terisi maka kosongkanlah
    $response['status'] = "error";
    $response['desc'] = "";
    $s_error  = "";
    $a_errors = array();

    if(isset($_REQUEST))
    {
        foreach ($_REQUEST as $key => $value) 
        {
            $_REQUEST[$key] = $oUser->antiInjection($value);
        }
    }


    if(isset($_REQUEST['action']))
    {



        //--------------------------------------------------
        //-----------WILAYAH ADMIN--------------------------
        //--------------------------------------------------

        if($_REQUEST['action'] == 'device-aktifasi')
        {
            if(isset($_REQUEST['device-token']))
            {

                $a_data_device = $oDevice->getList(" WHERE DUR.firebaseToken = '{$_REQUEST['device-token']}' ","","");
                if(count($a_data_device)>0)
                {

                    if($oDevice->activationDeviceRegister($a_data_device[0]['durID'],"1"))
                    {

                        $s_where_user = " WHERE firebaseToken = '{$_REQUEST['TOKEN_KEY']}' ";
                        $a_data_user = $oUser->getUserList($s_where_user,"","");
                        $message = "perangkat anda telah diaktifkan oleh ".$a_data_user[0]['realName'];
                        if($oNotif->create(

                            "register device" ,
                            $message ,
                            "" ,
                            $a_data_device[0]['userID'] ,
                            $a_data_device[0]['firebaseToken'] ,
                            $a_data_user[0]['userID'] ,
                            ""
                        ))
                        {

                            $response['status'] = 'success';
                            $response['desc'] = "perangkat  berhasil diaktifkan";


                            $a_firebase_data[0]['notif-to-firebase-id'] = $a_data_device[0]['firebaseToken']; 
                            $a_firebase_data[0]['notif-id'] = $oNotif->getCount("");
                            $a_firebase_data[0]['notif-body'] = $message;  
                            $a_firebase_data[0]['notif-title'] = "PERANGKAT AKTIF";
                            

                            $a_checksum['total']= 0;
                            $a_checksum['error'] = 0;
                            $a_checksum['success'] = 0;

                            //menggunakan config di app
                            @include(str_replace("/class", "/config.php", $SYSTEM['DIR_MODUL_CLASS']));
                            cron_send_notification($a_firebase_data,$SYSTEM,0,$a_checksum);
                        }
                        else
                        {
                            $response['status'] = 'error';
                            $response['desc'] = "ada error waktu notifikasi";
                        }
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['desc'] = "perangkat tidak dapat diaktifkan";
                    }
                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "perangkat tidak ditemukan";
                }
            }
            else
            {

                $response['status'] = 'error';
                $response['desc'] = "token perangkat tidak ada";
            }
        }
        elseif($_REQUEST['action'] == 'token-info')
        {
            if(isset($_REQUEST['device-token']))
            {

                $a_data_device = $oDevice->getList(" WHERE DUR.firebaseToken = '{$_REQUEST['device-token']}' ","","");
                if(count($a_data_device) > 0)
                {

                    $response['status'] = 'success';
                    $response['desc'] = "apakah anda akan mengaktifkan akun {$a_data_device[0]['realName']} di perangkat {$a_data_device[0]['deviceModel']}?";
                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "perangkat tidak ditemukan";
                }
            }
            else
            {

                $response['status'] = 'error';
                $response['desc'] = "token tidak ditemukan";
            }
        }
        //-----------END WILAYAH ADMIN----------------------
    }

    if ($a_errors) 
    {
        $s_error =  '';
        foreach ($a_errors as $error) 
        {
            $s_error .= "$error<br />";
        }
        $response['status'] = 'error';
        $response['desc'] = $s_error;
    }
    $oUser->closeDB();
    $oGroup->closeDB();
    $oDevice->closeDB();
    $oNotif->closeDB();
?>