<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    $PAGE_ID="LOGIN";
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.client.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.log.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.firewall.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.phone.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.app.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.tokenhandle.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.validation.php");
    $oApp = new Application();
    $oUser = new UserInfo();
    $oLog = new Log();
    $oClient = new Client();
    $oPhone = new Phone();
    $oTokenHandle = new TokenHandle();
    $oValidation = new Validation();
    
    //jika variable error sudah terisi maka kosongkanlah
    $response['status'] = "error";
    $response['desc'] = "belum ada respon";
    $s_error  = "";
    $a_errors = array();
    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == "register-device")
        {
            //variabel yang wajib diisi
            $array_var[0]['index-name'] =   "app-name";;
            $array_var[0]['not-isset']  =   "nama aplikasi tidak tercantum";
            $array_var[0]['is-blank']   =   "nama aplikasi harus diisi";

            $array_var[1]['index-name'] =   "app-version";;
            $array_var[1]['not-isset']  =   "versi aplikasi tidak tercantum";
            $array_var[1]['is-blank']   =   "versi aplikasi harus diisi";


            $array_var[2]['index-name'] =   "email";
            $array_var[2]['not-isset']  =   "email tidak tercantum";
            $array_var[2]['is-blank']   =   "email harus diisi";

            $array_var[3]['index-name'] =   "device-model";
            $array_var[3]['not-isset']  =   "model hp tidak tercantum";
            $array_var[3]['is-blank']   =   "model hp  harus diisi";

            $array_var[4]['index-name'] =   "device-sdk";
            $array_var[4]['not-isset']  =   "versi SDK  tidak tercantum";
            $array_var[4]['is-blank']   =   "versi SDK  harus diisi";

            $array_var[5]['index-name'] =   "device-merk";
            $array_var[5]['not-isset']  =   "merk hp   tidak tercantum";
            $array_var[5]['is-blank']   =   "merk hp harus diisi";


            //imei atau uuid
            $array_var[6]['index-name'] =   "unique-identifier";
            $array_var[6]['not-isset']  =   "id hp   tidak tercantum";
            $array_var[6]['is-blank']   =   "id hp harus diisi";

            //variabel yang tidak wajib diisi divalidasi di class





            $a_errors = $oValidation->check($_REQUEST,$array_var);
            /*
            for($i=0;$i<count($array_var);$i++)
            {
                if(!isset($_REQUEST[$array_var[$i]['index-name']]))
                {
                    $a_errors[] =   $array_var[5]['not-isset'];
                }
                elseif($_REQUEST[$array_var[$i]['index-name']] == "")
                {
                    $a_errors[] =   $array_var[5]['is-blank'];
                }
            }
            */





            if (!$a_errors) 
            {

                $a_data = [];
                foreach ($_REQUEST as $key => $value) 
                {
                    $a_data[$key] = $value; 
                } 

                //periksa nama dan versi aplikasi apakah sudah ada disistem
                $s_where_app = " WHERE appName = '{$_REQUEST['app-name']}' ";
                $a_data_app = $oApp->getList($s_where_app,"","");
                if(isset($a_data_app))
                {
                    if(count($a_data_app)>0)
                    {
                        if($_REQUEST['app-version'] >= $a_data_app[0]['lastVersion'])
                        {

                            //cek apakah ada di pengguna
                            $s_where_user = " WHERE mail = '{$_REQUEST['email']}' ";
                            if($oUser->getUserCount($s_where_user)> 0)
                            {
                                $a_user = $oUser->getUserList($s_where_user,"","");
                                $s_where_device = " WHERE `userID` = '{$a_user[0]['userID']}' AND deviceModel =   '{$_REQUEST['device-model']}' AND registerStatus = 0";
                                if($oPhone->getCount($s_where_device) > 0)
                                {

                                    $response['status'] = 'error';
                                    $response['desc'] = "device ini sudah pernah didaftarkan dan sedang menunggu aktifasi dari helpdesk";
                                }
                                else
                                {
                                    //membuat register untuk dimasukan sistem
                                    //manambah variabel yang belum ada di $_REQUEST
                                    $request['user-id']             = $a_user[0]['userID'];
                                    $request['registration-date']   = date("Y-m-d H:i:s");

                                    foreach ($_REQUEST as $key => $value) 
                                    {
                                        $request[$key]  =   $value;
                                    }

                                    //buat token
                                    $date_expired = date('Y-m-d H:i:s', strtotime(' + 5 days'));
                                    if
                                    (
                                        //update firebase token
                                        $oTokenHandle->create($a_user[0]['userID'],$date_expired)
                                    )
                                    {
                                        
                                        $request['registration-token'] = $oTokenHandle->lastToken();
                                        if($oPhone->create($request))
                                        {
                                            $response['status']                 = 'success';
                                            $response['desc']                   = "pengguna sudah dimasukan dalam antrian akun";
                                            $response['registration-token']     = $request['registration-token'];
                                            $response['registration-status']    = "0";
                                        }
                                        else
                                        {

                                            $response['status']             = 'error';
                                            $response['desc']               = "gagal dalam memasukan antrian akun";
                                        }

                                    }
                                    else
                                    {
                                        $response['status'] = 'error';
                                        $response['desc'] = "tidak dapat membuat token";

                                    }
                                }
                                
                                    
                            }
                            else
                            {
                                $response['status'] = 'error';
                                $response['desc'] = "email tidak ditemukan";

                            } 

                        }
                        else
                        {
                            $response['status'] = 'error';
                            $response['desc'] = "versi aplikasi sudah kadaluarsa. harap download apk yang terbaru.";

                        }
                    }
                    else
                    {

                        $response['status'] = 'error';
                        $response['desc'] = "aplikasi ini tidak terdaftar didalam sistem. harap mengunduh apk resmi dari helpdesk.";
                    }
                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "aplikasi yang anda install bukan resmi dari helpdesk. harap download dan install ulang aplikasi resminya";

                }

                            
            }
        }

        elseif ($_REQUEST['action'] == 'imei-check') 
        {    
            //periksa imei
            $s_where_device = " WHERE PR.`deviceID` = '{$_REQUEST['device-network-carrier']}'  ";
            $a_data = [];
            $a_data  = $oPhone->getList($s_where_device,"","");
            if(isset($a_data))
            {
                if(count($a_data) > 0)
                {
                    if( $a_data[0]['mail'] == $_REQUEST['email'] )
                    {
                        //sudah aktifasi
                        if($a_data[0]['registerStatus']  == "1")
                        {
                            $response['token-login']    = $a_data[0]['loginToken'];
                            $response['desc']           = "akun sudah aktif";

                        }
                        else
                        {
                            $response['desc'] = "menunggu aktifasi admin";

                        }
                        $response['status']             = 'success';
                        $response['real-name']          = $a_data[0]['realName'];


                        

                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['desc'] = "perangkat ini sudah pernah dikaitkan dengan akun lain.harap periksa ulang pengguna dan email jika memang perangkat ini milik anda. ";
                        
                    }
                } 
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "imei tidak terdaftar didatabase";

                }
            }
            else
            {
                $response['status'] = 'error';
                $response['desc'] = "imei belum terdaftar didatabase";

            }
        }

        elseif($_REQUEST['action'] == 'check-waiting')
        {
            print_r($_REQUEST);exit();
            if($_REQUEST['registration-token'] == "")
            {
                $a_errors[] = "registration token masih kosong";
            }
            if (!$a_errors) 
            {
                $s_where_device = " WHERE PR.`registrationToken` = '{$_REQUEST['registration-token']}' ";
                //echo $s_where_device ;
                $a_data  = $oPhone->getList($s_where_device,"","");
                if(count($a_data ) > 0)
                {
                    if($a_data[0]['registerStatus'] == 1)
                    {
                        $response['status'] = 'success';
                        $response['desc'] = "akun sudah aktif";
                        $response['registration-status'] = "1";
                        $response['realname'] = $a_data[0]['realName'];

                       
                    }
                    elseif($a_data[0]['registerStatus'] == 0)
                    {

                        $response['status'] = 'error';
                        $response['desc'] = "register anda sudah masuk dalam daftar tunggu. harap menghubungi helpdesk";
                    }
                    elseif($a_data[0]['registerStatus'] == 2)
                    {

                        $response['status'] = 'error';
                        $response['desc'] = "dikarenakan anda menginstal ulang / akun dinonaktifkan, anda harus mengunggu admin untuk mengaktifkan ulang";
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['desc'] = "akun anda sudah expired. harap install ulang aplikasi anda";
                    
                    }
                }
                else
                {
                    $response['status'] = 'error';
                    $response['desc'] = "token tidak terdaftar didatabase";

                }
            }
        }
    }

    if ($a_errors) 
    {
        $s_error =  '';
        foreach ($a_errors as $error) 
        {
            $s_error .= "$error<br />";
        }
        $response['status'] = 'error';
        $response['desc'] = $s_error;
    }
    $oUser->closeDB();
    $oLog->closeDB();
    $oPhone->closeDB();
    $oApp->closeDB();
    $oTokenHandle->closeDB();


?>