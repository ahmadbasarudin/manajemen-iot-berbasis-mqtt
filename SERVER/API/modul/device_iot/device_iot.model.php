<?php
    /*
    |--------------------------------------------------------------------------
    | Ticket
    |--------------------------------------------------------------------------
    |model  modul ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.device_iot.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/function.firebase.php");


    $oDeviceIot = new DeviceIot();
    $oUser = new UserInfo();

    $a_errors = array();
    $response['result'] = 'error';
    $response['desc'] = "tidak tereksekusi";

    //--------------config--------------
    $limit = 100; // Jumlah data per halamannya
    //--------------end config--------------

    if(isset($_REQUEST['action']))
    {
        if(isset($_REQUEST['TOKEN_KEY']))
        {
            if($_REQUEST['TOKEN_KEY'] == "")
            {
                $response['result'] = 'error';
                $response['desc'] = "TOKEN kosong";
            }
            else
            {

                $_REQUEST['TOKEN_KEY'] = $oDeviceIot->antiInjection($_REQUEST['TOKEN_KEY']);
                $s_where_user = " WHERE firebaseToken = '{$_REQUEST['TOKEN_KEY']}' ";
                if($oUser->getUserCount($s_where_user) > 0)
                {
                    $a_data_user = $oUser->getUserList($s_where_user ,"","");
                    if($_REQUEST['action'] == 'list')
                    {

                        // Cek apakah terdapat data page pada URL
                        $page = (isset($_POST['query_page']))? $_POST['query_page'] : 1;
                        //$no = (($page - 1) * $limit) + 1; // Untuk setting awal nomor pada halaman yang aktif
                        // Untuk menentukan dari data ke berapa yang akan ditampilkan pada tabel yang ada di database
                        $limit_start = ($page - 1) * $limit;
                        $query_search = "";
                        if(isset($_POST['search']) )
                        {
                            if($_POST['search'] != "")
                            {
                                $_POST['search'] = $oDeviceIot->antiInjection($_POST['search']);
                                $query_search = " 
                                                AND  
                                                    (
                                                        diID like '%".$_POST['search']."%' 
                                                        OR 
                                                        diToken  like '%".$_POST['search']."%'
                                                        OR 
                                                        diToken  like '%".$_POST['diPelangganPasca']."%'
                                                    ) 
                                                ";   
                            }
                        }

                        $query_limit = "LIMIT ".$limit_start.",".$limit;


                        $query_where = " WHERE diOwner='{$a_data_user[0]['userID']}' ";
                        $a_data = $oDeviceIot->getList($query_where . $query_search, "  ORDER BY `diDateCreate` DESC ",$query_limit);
                        

                        $is_last_page = false;
                        if(($limit_start + $limit) >  $oDeviceIot->getCount($query_where.$query_search))
                        {
                            $is_last_page = true;
                        }

                        $response['result'] = 'success';
                        $response['desc'] = "";

                        $response['limit'] = $limit;
                        $response['page'] = $page;
                        $response['is_last_page'] = $is_last_page;
                        $response['total'] = $oDeviceIot->getCount($query_where.$query_search);

                        $response['data'] = $a_data;   

                    }
                    elseif($_REQUEST['action'] == 'customer')
                    {
                        if(!isset($_REQUEST['customer']))
                        {
                            $a_errors[] = "Pelanggan harus diisi";
                        }
                        elseif(strlen($_REQUEST['customer']) != 12)
                        {
                            $a_errors[] = "ID pelanggan harus 12 digit";
                        }   

                        if(!isset($_REQUEST['device-id']))
                        {
                            $a_errors[] = "ID perangkat tidak ada";
                        }

                        if (!$a_errors) 
                        {
                            $data['diID']       = $_REQUEST['device-id'];
                            $data['customer']   = $_REQUEST['customer'];
                            if($oDeviceIot->customer($data))
                            {
                                $response['result'] = 'success';
                                $response['desc'] = "id pelanggan berhasil dimasukan";

                            }
                            else
                            {
                                $response['result'] = 'error';
                                $response['desc'] = "id pelanggan gagal dimasukan";
                            }

                        }
                    }
                }
                else
                {
                    $response['result'] = 'error';
                    $response['desc'] = "TOKEN tidak ada dalam database";
                }   
            }
        }
        else
        {
            $response['result'] = 'error';
            $response['desc'] = "TOKEN tidak ada";

        }
    }

    if ($a_errors) 
    {
        $s_error =  '';
        foreach ($a_errors as $error) 
        {
            $s_error .= "$error.";
        }
        $response['status'] = 'error';
        $response['desc'] = $s_error;
    }
    echo json_encode($response);
    $oDeviceIot->closeDB();
    $oUser->closeDB();

?>