<?php
/*
|--------------------------------------------------------------------------
| Config
|--------------------------------------------------------------------------
|
|Config system
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    $APP =  "DEV"; //DEV atau PRO

    if($APP == "DEV") //develop
    {   
        //------------------------------------------------
        DEFINE("DB_USER" , "root");
        DEFINE("DB_PASSWORD" ,"");
        DEFINE("DB_NAME" ,"iot_mqtt");
        DEFINE("DB_HOST" , "localhost");
        $SYSTEM['DIR_APP'] = "/Users/basar/Desktop/manajemen-iot-berbasis-mqtt/SERVER/APP";
        $SYSTEM['DIR_PATH'] = "/Users/basar/Desktop/manajemen-iot-berbasis-mqtt/SERVER/API";
        //------------------------------------------------
    }
    else if($APP == "PRO" ) // production
    {
        //------------------------------------------------
        DEFINE("DB_USER" , "u3966337_bossman");
        DEFINE("DB_PASSWORD" ,"b055m4n");
        DEFINE("DB_NAME" ,"u3966337_matrik");
        DEFINE("DB_HOST" , "localhost");
        $SYSTEM['DIR_APP'] = "/home/u3966337/public_html/sistem-manajemen-mqtt-device/SERVER/APP";
        $SYSTEM['DIR_PATH'] = "/home/u3966337/public_html/sistem-manajemen-mqtt-device/SERVER/API";
        //------------------------------------------------

    }
  


    DEFINE("SUPER_ADMIN_GROUP","SUPER_ADMINISTRATOR");
    DEFINE("LOGIN_REFERENCE","PASSWORD");


    
    $SYSTEM['DIR_MODUL_CLASS'] = $SYSTEM['DIR_APP']."/class";
    //$SYSTEM['DIR_PATH'] = "/opt/lampp/htdocs/simontok-api";
    //$SYSTEM['DIR_PATH'] = "D:/xampp/htdocs/simontok-api";
    $SYSTEM['DIR_MODUL'] = $SYSTEM['DIR_PATH'] ."/modul";
    $SYSTEM['DIR_MODUL_CORE'] = $SYSTEM['DIR_MODUL'] ."/core";


    $SYSTEM['FIREWALL'] ='OFF'; //OFF, ON


?>